#ifdef _WIN32
	#include <windows.h>
	#include <winnls.h>
	#include <tchar.h>
#elif __APPLE__
	#include <unistd.h>
#endif

#include <locale>
#include <cstdio>
#include <cstdlib>
#include <string>

#ifndef NOCPUID
#include <cpuid.h>
#endif /// of NOCPUID

#include "dlgMain.h"

#include <FL/Fl_Tooltip.H>

////////////////////////////////////////////////////////////////////////////////

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_CLSNAME         "MRAWVIEWER"

////////////////////////////////////////////////////////////////////////////////

bool testing_cpu()
{
#ifndef NOCPUID
    unsigned int eax = 0;
    unsigned int ebx = 0;
    unsigned int ecx = 0;
    unsigned int edx = 0;
    unsigned int max_level = 0;
    unsigned int ext_level = 0;
    unsigned int vendor = 0;
    unsigned int model = 0;
    unsigned int family = 0;

    // related in Core2Duo or DualCore.
    unsigned int has_sse3 = 0;
    unsigned int has_ssse3 = 0;
    unsigned int has_cmpxchg8b = 0;
    unsigned int has_cmov = 0;
    unsigned int has_mmx = 0;
    unsigned int has_sse = 0;
    unsigned int has_sse2 = 0;

    // related in Core I series and AVX.
    unsigned int has_movbe = 0;
    unsigned int has_sse4_1 = 0;
    unsigned int has_sse4_2 = 0;
    unsigned int has_avx = 0;

    max_level = __get_cpuid_max (0, &vendor);
    if (max_level < 1)
        return false;

    __cpuid (1, eax, ebx, ecx, edx);

    model = (eax >> 4) & 0x0f;
    family = (eax >> 8) & 0x0f;

    if ( vendor == signature_INTEL_ebx || vendor == signature_AMD_ebx )
    {
        unsigned int extended_model, extended_family;

        extended_model = (eax >> 12) & 0xf0;
        extended_family = (eax >> 20) & 0xff;

        if (family == 0x0f)
        {
            family += extended_family;
            model  += extended_model;
        }
        else
        if (family == 0x06)
        {
            model += extended_model;
        }

        has_cmpxchg8b = edx & bit_CMPXCHG8B;
        has_cmov = edx & bit_CMOV;
        has_mmx = edx & bit_MMX;
        has_sse = edx & bit_SSE;
        has_sse2 = edx & bit_SSE2;

        has_sse3 = ecx & bit_SSE3;
        has_ssse3 = ecx & bit_SSSE3;
        has_sse4_1 = ecx & bit_SSE4_1;
        has_sse4_2 = ecx & bit_SSE4_2;
        has_avx = ecx & bit_AVX;

#ifdef __SSE3__
        if ( has_sse3 == 0 )
            return false;
#endif /// of __SSE3__

#ifdef __SSE4_1__
        if ( has_sse4_1 == 0 )
            return false;
#endif /// of __SSE4_1__

#ifdef __SSE4_2__
        if ( has_sse4_2 == 0 )
            return false;
#endif /// of __SSE4_2__

#ifdef __AVX__
        if ( has_avx == 0 )
            return false;
#endif /// of __AVX__

        return true;
    }

    return false;
#else
	return true;
#endif /// of __APPLE__
}

void procAutoLocale()
{
#ifdef _WIN32
    LANGID currentUIL = GetSystemDefaultLangID();

#ifdef DEBUG
    printf("current LANG ID = %08X ( %d )\n", currentUIL, currentUIL );
#endif // DEBUG

    const char* convLoc = NULL;

    switch( currentUIL & 0xFF )
    {
        case LANG_KOREAN:
            convLoc = "korean";
            break;

        case LANG_JAPANESE:
            convLoc = "japanese";
            break;

        case LANG_CHINESE:
            convLoc = "chinese";
            break;

        default:
            convLoc = "C";
            break;
    }
#else
	const char convLoc[] = "C";
#endif /// of _WIN32
    setlocale( LC_ALL, convLoc );
}

void presetFLTKenv()
{
#ifdef _WIN32
	Fl::set_font( FL_FREE_FONT, "Tahoma" );
#else
	//Fl::set_font( FL_FREE_FONT, "Noto Sans Regular" );
	Fl::set_font( FL_FREE_FONT, FL_HELVETICA );
#endif

    Fl_Double_Window::default_xclass( DEF_APP_CLSNAME );

    Fl_Tooltip::color( fl_darker( FL_DARK3 ) );
    Fl_Tooltip::textcolor( FL_WHITE );
    Fl_Tooltip::size( 11 );
    Fl_Tooltip::font( FL_FREE_FONT );

    Fl::scheme( "flat" );
    Fl::lock();
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char ** argv)
{
    double flv = Fl::version();

#ifdef DEBUG
    printf( "FLTK version: %f\n", flv );
#endif // DEBUG

    if( flv < 1.030400 ) /// It must be above 1.3.4
        return 0;

    if ( testing_cpu() == false )
    {
#ifdef _WIN32
        MessageBeep( MB_ICONERROR );
        MessageBox( 0,
                    TEXT("Your CPU architecture is not supported for current build of program.\n"
                         "Please choose right CPU model binary with this appendix reference:\n"
                         "   - no appendix: Normal i686 instructions.\n"
                         "   - '_sse3' appendix: Intel Core2Duo or DualCores\n"
                         "   - '_avx' appendix: After 3th Generation Intel Core i5/7\n" ),
                    TEXT("Unsupported CPU notice:"),
                    MB_ICONERROR );
#else
#endif /// of _WIN32
        return 0;
    }

    presetFLTKenv();

    DialogMain* dlgMain = new DialogMain( argc, argv );

    int retCode = 0;

    if ( dlgMain != NULL )
    {
        procAutoLocale();

        retCode = dlgMain->Run();
    }

    return retCode;
}
