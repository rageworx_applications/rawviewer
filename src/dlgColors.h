#ifndef __DLGCOLORS_H__
#define __DLGCOLORS_H__

#define COL_WINDOW          0x44444400
#define COL_TEXT            0xAAAAAA00
#define COL_TEXTDIMMED      0x55555500
#define COL_CURSOR          FL_WHITE
#define COL_LINE            FL_DARK3
#define COL_IMGDSPBOX       FL_DARK1
#define COL_OUTLINE         FL_DARK1
#define COL_BUTTON          FL_DARK1
#define COL_BUTTONDIMMED    FL_DARK3
#define COL_LOG             FL_BLACK
#define COL_LOG_TEXT        FL_GREEN
#define COL_INPUT           FL_BLACK

#endif /// of __DLGCOLORS_H__
