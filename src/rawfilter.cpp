#include "rawfilter.h"
#include <math.h>

using namespace std;

void basicfilter16::dofilter16( int width, int height,
                                vector<unsigned short> &src,
                                vector<unsigned short> &dst )
{
    double z = 10.0f;
    double mask[] = { 0,   -z  ,   0,
                     -z,  1+4*z,  -z,
                      0,   -z  ,   0 };

    for(int i=1;i<width-1;i++)
    {
       for(int j=1;j<height-1;j++)
       {
            double linc_p = 0.0f;

            for(int k=0; k<=2; k++)
            {
                for(int l=0; l<=2; l++)
                {
                    linc_p = src[ ( ( i+k-1 ) * width ) + j+l-1 ] * mask[ k*3 + l ];
                }
            }

            dst.push_back( round( linc_p ) );
       }
    }
}
