#ifdef __APPLE__
    #include <sys/uio.h>
    #include <unistd.h>
#elif __linux__
	//if you got some error on refer to io.h,
    //uncomment here:
    //#include <sys/io.h>
	#include <unistd.h>
#else
    #include <io.h>
#endif // __APPLE__

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <algorithm>
#include <cmath>

#include "stdunicode.h"

#include "dlgColors.h"
#include "dlgCoords.h"
#include "dlgMain.h"

#include <FL/Fl_Native_File_Chooser.H>
#include <FL/fl_ask.H>
#include <FL/Fl_Image.H>
#include <FL/fl_draw.H>

#ifndef _WIN32
    #include <FL/fl_ask.H>
#endif

#include "stdunicode.h"
#include "libdcm.h"
#include "perfmon.h"
#include "minIni.h"

#include "resource.h"

using namespace std;

////////////////////////////////////////////////////////////////////////////////

#define STR_APP_TITLE       "Rageworx' Medi-Raw Image Viewer (w/FLTK&PTHREAD)"
#define DEF_MAX_PATH        256
#define DEF_FONT_SIZE       11
#define DEF_D_NAME          ".MRawViewer"
#define DEF_D_INI_FILE      "MRawViewer.ini"
#define MSG_INFO "RAGEWORX's Medical RAW Image Viewer [ Engineering Sample Version ]\n" \
                  "(C)Copyright 2013~2016 Rageworx software ,Raphael Kim (rageworx@@gmail.com)\n\n" \
                  "* This project used libfltk(1.3.4-ts), and it is a part of open source libFLTK\n" \
                  "* This project used libtinydicom, libminini, and these are part of open source.\n"
#define DEF_STR_WAIT4CALC   "...WAIT WHILE CALCULATING..."
#define DEF_STR_WAIT4SAVE   "...WAIT WHILE SAVING IMAGE..."

#define DEFAULT_IMAGE_HEIGHT    3072

#ifdef UNICODE
    #define _TSTRING        wstring
    #define _TCM2W( _x_ )   convertM2W( (const char*)_x_ )
    #define _TCW2M( _x_ )   convertW2M( (const wchar_t*)_x_ )
#else
    #define _TSTRING        string
    #define _TCM2W( _x_ )   _x_
    #define _TCW2M( _x_ )   _x_
#endif

#define GROUP_BEGIN( _x_ )          if( _x_ != NULL ) _x_->begin()
#define GROUP_END( _x_ )            if( _x_ != NULL ) _x_->end()
#define GROUP_RESIZABLE( _x_, _y_ ) if ( ( _x_ != NULL ) && ( _y_ != NULL ) ) _x_->resizable( _y_ )

#ifndef F_OK
	#define F_OK	0
#endif

////////////////////////////////////////////////////////////////////////////////

static char currentPath[DEF_MAX_PATH] = {0};
static char openPath[DEF_MAX_PATH] = {0};

////////////////////////////////////////////////////////////////////////////////

static void  fl_widget_cb(Fl_Widget* w, void* p);
static void  fl_timer_cb( void* p );
static void  preload_cb( void* p );
static void* pt_zoom_cb( void* p );
static void* pt_exportimg_cb( void* p );

////////////////////////////////////////////////////////////////////////////////

Fl_Menu_Item menuSaveImage[] =
{
  { "... to PNG\t", FL_ALT + 'p', fl_widget_cb, NULL, 0, 0, FL_FREE_FONT, DEF_FONT_SIZE, COL_TEXT },
  { "... to RAW\t", FL_ALT + 'r', fl_widget_cb, NULL, 0, 0, FL_FREE_FONT, DEF_FONT_SIZE, COL_TEXT },
  0
};

////////////////////////////////////////////////////////////////////////////////

#ifdef _WIN32
typedef int(__stdcall *MSGBOXWAPI)(IN HWND, IN LPCWSTR, IN LPCWSTR, IN UINT, IN WORD, IN DWORD);

int X_MessageBoxTimeout(HWND hWnd, const TCHAR* sText, const TCHAR* sCaption, UINT uType, DWORD dwMilliseconds)
{
    int retI = 0;

    HMODULE hUser32 = LoadLibraryA( "user32.dll" );

    if ( hUser32 != NULL )
    {
#ifdef UNICODE
        auto MessageBoxTimeoutW = (MSGBOXWAPI)GetProcAddress(hUser32, "MessageBoxTimeoutW");
        retI = MessageBoxTimeoutW(hWnd, sText, sCaption, uType, 0, dwMilliseconds);
#else
        auto MessageBoxTimeoutW = (MSGBOXWAPI)GetProcAddress(hUser32, "MessageBoxTimeoutA");
        retI = MessageBoxTimeoutA(hWnd, sText, sCaption, uType, 0, dwMilliseconds);
#endif
        FreeLibrary( hUser32 );
    }
    else
    {
        retI = MessageBox(hWnd, sText, sCaption, uType);
    }

    return retI;
}
#endif // _WIN32

////////////////////////////////////////////////////////////////////////////////

#ifdef _WIN32
static HICON hIconWindowLarge = NULL;
static HICON hIconWindowSmall = NULL;
#endif // _WIN32

////////////////////////////////////////////////////////////////////////////////

#if defined(__APPLE__) || !defined(_WIN32)
void itoa( int i, char* s, int t )
{
	switch( t )
	{
        case 8:
            sprintf( s, "%o", i );
            break;

		case 10:
			sprintf( s, "%d", i );
			break;

        case 16:
            sprintf( s, "%x", i );
            break;
	}
}
#endif

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

DialogMain::DialogMain(int argc, char** argv)
 : _argc( argc ),
   _argv( argv ),
   window(NULL),
   mediRAW(NULL),
   downscaled_raw_buffer(NULL),
   proceedingredraw( false ),
   needpreload(false),
   userevered( false ),
   userzoom( false ),
   workingnow( false ),
   nowloading( false ),
   loadtypeDCM( false ),
   preloadheight( -1 ),
   rotatetype( 0 ),
   rotatemask( TRANSFORM_NONE ),
   useinverted( false ),
   v_thld_min( 0 ),
   v_thld_max( 0 ),
   ucol_enabled( false ),
   pt_id_zoom( 0 )
{
    string argvextractor = argv[0];

#ifdef DEBUG
    char splitter[] = "/";
#else
    char splitter[] = "\\";
#endif

#ifdef _WIN32
    if ( argvextractor.size() > 0 )
    {
        string::size_type lastSplitPos = argvextractor.rfind( splitter );

        string extracted = argvextractor.substr(0, lastSplitPos + 1 );

        strcpy( currentPath, extracted.c_str() );
		strcpy( openPath, currentPath );
    }
#else
	sprintf( openPath, "%s", getenv("HOME") );
	sprintf( currentPath,
			 "%s/%s/",
			 openPath,
			 DEF_D_NAME );
	if ( access( currentPath, F_OK ) != 0 )
	{
		mkdir( currentPath, 0775 );
	}
#endif /// of _WIN32

    for( int cnt=1;cnt<argc;cnt++ )
    {
        string param = argv[cnt];
        if ( param.size() > 0 )
        {
            if ( param.at(0) == '-')
            {
                string::size_type poscmd = string::npos;

                // image height : -h1054
                poscmd = param.find("-h");
                if ( poscmd != string::npos )
                {
                    string strH = param.substr( poscmd + 2 );
                    if ( strH.size() > 0 )
                    {
                        preloadheight = atoi( strH.c_str() );
                    }
                }
            }
            else
            {
                if( access( param.c_str(), F_OK ) == 0 )
                {
                    preloadfilename = param;
                    needpreload = true;
                }
            }
        }
    }

    loadConfig();

#ifdef _WIN32
    if ( hIconWindowLarge == NULL )
    hIconWindowLarge = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         64,
                                         64,
                                         LR_SHARED );

    if ( hIconWindowSmall == NULL )
    hIconWindowSmall = (HICON)LoadImage( fl_display,
                                         MAKEINTRESOURCE( IDC_ICON_A ),
                                         IMAGE_ICON,
                                         16,
                                         16,
                                         LR_SHARED );
#endif // _WIN32

    window = new Fl_Double_Window( COORD_WINDOW );

    if ( window != NULL )
    {
        char titlestrmap[256] = {0};
        char cpuinststrmap[256] = {0};
		int  ver_is[4] = {APP_VERSION};

		sprintf( titlestrmap,
				 "%s v%d.%d.%d.%d",
				 STR_APP_TITLE,
				 ver_is[0], ver_is[1], ver_is[2], ver_is[3] );

#ifdef __SSE3__
        strcat( cpuinststrmap, " SSE3");
#endif // __SSE3__

#ifdef __SSSE3__
        strcat( cpuinststrmap, " SSSE3");
#endif // __SSSE3__

#ifdef __SSE4_1__
        strcat( cpuinststrmap, " SSE4.1");
#endif // __SSE4_1__

#ifdef __SSE4_2__
        strcat( cpuinststrmap, " SSE4.2");
#endif // __SSE4_1__

#ifdef __AVX__
        strcat( cpuinststrmap, " AVX");
#endif // __AVX__

        if ( strlen( cpuinststrmap ) > 0 )
        {
            strcat ( titlestrmap, " [");
            strcat ( titlestrmap, cpuinststrmap );
            strcat ( titlestrmap, " ]");
        }
        else
        {
            strcat ( titlestrmap, "" );
        }

#ifdef DEBUG
        strcat( titlestrmap, " -- *DEBUG*" );
#endif // DEBUG

        window->label( titlestrmap );
        window->color( COL_WINDOW );

#ifdef DEBUG
        printf( "createComponents();\n" );
        fflush( stdout );
#endif 
        window->begin();
        createComponents();
        window->end();
#ifdef DEBUG
        printf( "done -- createComponents();\n" );
        fflush( stdout );
#endif 
 
        window->callback( fl_widget_cb, this );
        window->resizable( imgDspBox );
        window->size_range( COORD_WINDOW_MIN );

        createSubCompoents();

        int winwh[2] = {COORD_WINDOW_MIN};

        if ( preloadw < winwh[0] )
        {
            preloadw = winwh[0];
        }

        if ( preloadh < winwh[1] )
        {
            preloadh = winwh[1];
        }

        if ( ( preloadw > winwh[0] ) || ( preloadh > winwh[1] ) )
        {
            window->resize( preloadx, preloady, preloadw, preloadh );
        }
        else
        {
            window->resize( preloadx, preloady, window->w(), window->h() );
        }

        if ( windowLog != NULL )
        {
            windowLog->position( preloadx + 10, preloady + 10 );
        }

        window->show();
        window->position( preloadx, preloady );

#ifdef _WIN32
        // This is for Windows application Icon.
        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_BIG,
                     (LPARAM)hIconWindowLarge );

        SendMessage( fl_xid( window ),
                     WM_SETICON,
                     ICON_SMALL,
                     (LPARAM)hIconWindowSmall );
#endif // _WIN32

        if( preloadheight > 0 )
        {
            if( inpHSize != NULL )
            {
                static char strpreloadH[13] = {0};
                sprintf( strpreloadH, "%d", preloadheight );
                inpHSize->value( strpreloadH );
            }
        }

        if ( needpreload == true )
        {
            window->deactivate();
            Fl::add_timeout(0.5f, preload_cb, this);
        }

        addLog( MSG_INFO );

        if ( strlen( cpuinststrmap ) > 0 )
        {
            addLog( "\n" );
            addLog( "CPU architecture model : " );
            addLog( cpuinststrmap );
            addLog( "\n\n" );
        }
    }

    Fl::add_timeout( 0.1f, fl_timer_cb, this );
#ifdef DEBUG
    printf( "DialogMain::DialogMain()\n" );
    fflush( stdout );
#endif
}

DialogMain::~DialogMain()
{
    Fl::remove_timeout( fl_timer_cb, this );

    if ( downscaled_raw_buffer != NULL )
    {
        delete[] downscaled_raw_buffer;
        downscaled_raw_buffer = NULL;
    }

    if ( mediRAW != NULL )
    {
        delete mediRAW;
    }
}

int DialogMain::Run()
{
    return Fl::run();
}

void DialogMain::ProcWidgetCB( Fl_Widget* w )
{
    if ( w == NULL )
        return;

    if ( w == btnLoadImage )
    {
        loadImage();
        return;
    }

    if ( w == cbUseThreshold )
    {
        cbUseThreshold->deactivate();
        thresholdControl();

        return;
    }

    if ( ( w == chsResizeType ) || ( w == vsImageZoom ) )
    {
        if ( downscaled_raw_buffer == NULL )
            return;

        double zoomv = vsImageZoom->value();

        if ( zoomv < ( vsImageZoom->minimum() + 0.06 ) )
        {
            vsImageZoom->value( vsImageZoom->minimum() + 0.1f );
            return;
        }

        if ( zoomv > ( vsImageZoom->maximum() - 0.04 ) )
        {
            vsImageZoom->value( vsImageZoom->maximum() - 0.1f );
            return;
        }

        grpMain->deactivate();
        grpOverlay->show();

        reserveRedraw();

        userzoom = true;

        //Create pthread
        int reti = pthread_create( &pt_id_zoom,
                                   NULL,
                                   pt_zoom_cb,
                                   this );

        if ( reti != 0 )
        {
            grpOverlay->hide();
            grpMain->activate();

            reserveRedraw();

#ifdef _WIN32
            MessageBox( fl_xid( window ),
                        TEXT( "Background work failure (pthread) !" ),
                        TEXT( "Error" ),
                        MB_ICONERROR );
#else
            fl_message_title( "Error" );
            fl_message_hotspot( 1 );
            fl_alert( "Background work failure (pthread) !" );
#endif // _WIN32
        }

        return;
    }

    if ( w == boxHistogram )
    {
        int clk_x = boxHistogram->lastclick_x();
        int clk_y = boxHistogram->lastclick_y();
        int clk_b = boxHistogram->lastclick_btn();

        if ( mediRAW != NULL )
        {
            float wratio = ( (float) mediRAW->MaximumLevel() /
                             (float) boxHistogram->w() );

            if ( clk_b == FL_LEFT_MOUSE )
            {
                unsigned test_w = wratio * clk_x;

                if ( test_w < weight_report.threshold_wide_max )
                {
                    weight_report.threshold_wide_min = test_w;

                    if ( weight_report.threshold_wide_min > weight_report.threshold_wide_max )
                    {
                        weight_report.threshold_wide_min = weight_report.threshold_wide_max - 1;
                    }
                }
            }
            else
            if ( clk_b == FL_RIGHT_MOUSE )
            {
                unsigned test_w = wratio * clk_x;

                if ( test_w > weight_report.threshold_wide_min )
                {
                    weight_report.threshold_wide_max = test_w;

                    if ( weight_report.threshold_wide_min >
                         weight_report.threshold_wide_max )
                    {
                        weight_report.threshold_wide_max = weight_report.threshold_wide_min + 1;
                    }
                }
            }

            boxHistogram->emphase_value( weight_report.threshold_wide_min,
                                         weight_report.threshold_wide_max );
            boxHistogram->redraw();
            thresholdControlModify( 1 );
        }

        return;
    }

    if ( w == inpImageZoomWidth )
    {
        // Calculate image size by width.

        if( mediRAW != NULL )
        {
            const char* refstr = inpImageZoomWidth->value();

            double newratio = 0.0;

            if ( strlen( refstr ) > 0 )
            {
                unsigned nw = atoi( refstr );
                unsigned ow = mediRAW->Width();

                newratio = (double)nw / (double)ow;

                if ( ( newratio < vsImageZoom->maximum() ) &&
                     ( newratio > vsImageZoom->minimum() ) )
                {
                    vsImageZoom->value( newratio );
                    vsImageZoom->do_callback();
                }
                else
                {
                    float newratio = 0.0f;

                    if ( newratio >= vsImageZoom->maximum() )
                    {

                        newratio = vsImageZoom->maximum() - 0.1f;

                    }
                    else
                    {
                        newratio = vsImageZoom->minimum() + 0.1f;
                    }

                    vsImageZoom->value( newratio );


                }
            }
            else
            {
                double orgratio = vsImageZoom->value();
                int orgw        = (double)mediRAW->Width() * orgratio;
                char orgwstr[32] = {0};

                itoa( orgw, orgwstr, 10 );

                inpImageZoomWidth->value( orgwstr );
            }
        }

        return;
    }

    if ( w == inpImageZoomHeight )
    {
        if( mediRAW != NULL )
        {
            const char* refstr = inpImageZoomHeight->value();

            double newratio = 0.0;

            if ( strlen( refstr ) > 0 )
            {
                unsigned nh = atoi( refstr );
                unsigned oh = mediRAW->Height();

                newratio = (double)nh / (double)oh;

                if ( ( newratio < vsImageZoom->maximum() ) &&
                     ( newratio > vsImageZoom->minimum() ) )
                {
                    vsImageZoom->value( newratio );
                    vsImageZoom->do_callback();
                }
                else
                {
                    if ( newratio >= vsImageZoom->maximum() )
                        vsImageZoom->value( vsImageZoom->maximum() - 0.1f );
                    else
                        vsImageZoom->value( vsImageZoom->minimum() + 0.1f );

                    vsImageZoom->value( newratio );
                    vsImageZoom->do_callback();
                }
            }
            else
            {
                double orgratio = vsImageZoom->value();
                int orgh        = (double)mediRAW->Height() * orgratio;
                char orghstr[32] = {0};

                itoa( orgh, orghstr, 10 );

                inpImageZoomHeight->value( orghstr );
            }
        }

        return;
    }

    if ( w == btnFitToWidth )
    {
        updateFitButtonState( 0 );
        return;
    }

    if ( w == btnFitToOrigin )
    {
        updateFitButtonState( 1 );
        return;
    }

    if ( w == inpHSize )
    {
        heightApply();
        return;
    }

    if ( w == btnCloseRawThumb )
    {
        //windowRawThumb->hide();
        windowRawThumb->do_callback();
        return;
    }

    if ( w == btnSaveImage )
    {
#ifdef DEBUG
		printf("#DEBUG# Callback : btnSaveImage..\n");
#endif

        if ( imgDspBox == NULL )
            return;

        if ( imgDspBox->image() == NULL )
            return;

        const Fl_Menu_Item* pMenu = btnSaveImage->mvalue();

        if ( pMenu != NULL )
        {
            grpMain->deactivate();
            boxDspWait->label( DEF_STR_WAIT4SAVE );
            grpOverlay->show();

            reserveRedraw();

            if ( pMenu == &menuSaveImage[0] ) /// ... save to PNG.
            {
                exportparameter = 0;
            }
            else
            if ( pMenu == &menuSaveImage[1] ) /// save to RAW.
            {
                exportparameter = 1;

                if ( mediRAW == NULL )
                    return;

                if ( mediRAW->Loaded() == false )
                    return;
            }

#ifdef DEBUG
			printf("#DEBUG# trying to create export thread ... \n");
#endif

            int reti = pthread_create( &pt_id_exportimage,
                                       NULL,
                                       pt_exportimg_cb,
                                       this );

            if ( reti != 0 )
            {
                grpOverlay->hide();
                boxDspWait->label( DEF_STR_WAIT4CALC );
                grpMain->activate();
                grpMain->redraw();

                reserveRedraw();

#ifdef _WIN32
                MessageBox( fl_xid( window ),
                            TEXT( "Exporting image background work failure (pthread) !" ),
                            TEXT( "Error" ),
                            MB_ICONERROR );
#else
                fl_message_title( "Error" );
                fl_message_hotspot( 1 );
                fl_alert( "Background work failure (pthread) !" );
#endif // _WIN32
            }

        }
        return;
    }

    if ( w == windowRawThumb )
    {
        windowRawThumb->hide();
        reserveRedraw();
        return;
    }

    if ( w == window )
    {
        /* ----------------------------------------
         * ESC not works well in FLTK 1.3.4-1, why?
         * ----------------------------------------

        int ekey = Fl::event_key();

        switch( ekey )
        {
            case FL_Escape:
                return;
        }
        */

        // Going close ...
        saveConfig();

        if ( windowRawThumb != NULL )
        {
            if ( windowRawThumb->visible_r() > 0 )
            {
                windowRawThumb->hide();
            }

            delete windowRawThumb;
            windowRawThumb = NULL;
        }

        if ( windowLog != NULL )
        {
            if ( windowLog->visible_r() > 0 )
            {
                windowLog->hide();
            }

            delete windowLog;
            windowLog = NULL;
        }

        window->hide();
        delete window;
    }
}

void* DialogMain::ProcZoomCB() /// in threaded...
{
#ifdef DEBUG
    printf( "[B]void* DialogMain::zoomProc()\n");
#endif // DEBUG

    workingnow = true;

    Fl::lock();

    int  rstype = chsResizeType->value();

    if ( rstype != imgDspBox->resizemethod() )
    {
        imgDspBox->resizemethod( rstype, false );
    }

    displayzoomratio();

#ifdef DEBUG
    printf( "---1---" );fflush(stdout);
#endif
    if ( grpOverlay->visible_r() > 0 )
    {
        grpOverlay->hide();
        grpOverlay->redraw();

#ifdef DEBUG
        printf( "---2---" );fflush(stdout);
#endif

        grpMain->activate();
        grpMain->redraw();
    }

#ifdef DEBUG
    printf( "[E]void* DialogMain::zoomProc()\n");
#endif // DEBUG

    Fl::unlock();
    Fl::awake();

    workingnow = false;

    return NULL;
}

void* DialogMain::ProcExportImgCB()
{
#ifdef DEBUG
	printf("#DEBUG# DialogMain::exportimageProc() ... \n");
#endif
    switch( exportparameter )
    {
        case 0: /// PNG.
            {
                Fl_Native_File_Chooser nFC;

                nFC.title( "Select PNG file to save." );
                nFC.type( Fl_Native_File_Chooser::BROWSE_SAVE_FILE );
                nFC.filter( "PNG Image\t*.png" );
                nFC.directory( openPath );
                nFC.preset_file(NULL);

                int retVal = nFC.show();

#ifdef _WIN32
                wstring refp = convertM2W( nFC.filename(0) );
#else
				string  refp = nFC.filename(0);
#endif /// of _WIN32

                if ( refp.size() == 0 )
                {
                    grpOverlay->hide();
                    boxDspWait->label( DEF_STR_WAIT4CALC );
                    grpMain->activate();
                    reserveRedraw();

                    return NULL;
                }

#ifdef _WIN32
                if ( refp.find( L'.' ) == wstring::npos )
                {
                    refp += L".png";
                }
#else
                if ( refp.find( L'.' ) == wstring::npos )
                {
                    refp += ".png";
                }
#endif /// of _WIN32

#ifdef _WIN32
                if ( _waccess( refp.c_str(), F_OK ) == 0 )
                {
                    if ( _wunlink( refp.c_str() ) != 0 )
                    {
                        MessageBox( fl_xid( window ),
                                    TEXT("Cannot write PNG file!\nMaybe access denied."),
                                    TEXT("ERROR:"),
                                    MB_ICONERROR );
#else
				if ( access( refp.c_str(), F_OK ) == 0 )
				{
					if ( unlink( refp.c_str() ) != 0 )
					{
                        fl_message_title( "Error" );
                        fl_message_hotspot( 1 );
                        fl_alert( "Cannot write PNG file!\nMaybe access denied." );
#endif // _WIN32
                        grpOverlay->hide();
                        boxDspWait->label( DEF_STR_WAIT4CALC );
                        grpMain->activate();
                        reserveRedraw();

                        return NULL;
                    }
                }

                if( imgDspBox->savetomonopng( refp.c_str() ) == true )
                {
#ifdef _WIN32
                    X_MessageBoxTimeout( fl_xid( window ),
                                         TEXT("Save PNG image file success.\n"
                                              "(This dialog automatically close in 5sec."),
                                         TEXT("Save to PNG"),
                                         MB_ICONINFORMATION,
                                         5000 );
#else
                    fl_message_title( "Save to PNG" );
                    fl_message_hotspot( 1 );
                    fl_message( "Save PNG image file success." );
#endif // _WIN32
                }
                else
                {
#ifdef _WIN32
                    MessageBox( fl_xid( window ),
                                TEXT("Save PNG image failure !\n"
                                     "Check file system access." ),
                                TEXT("Save to PNG"),
                                MB_ICONERROR );
#else
                    fl_message_title( "Save to PNG" );
                    fl_message_hotspot( 1 );
                    fl_alert( "Save PNG image failure !\nCheck file system access." );
#endif // _WIN32
                }

            }
            break;

        case 1: /// RAW.
            {
                Fl_Native_File_Chooser nFC;

                nFC.title( "Select RAW/IMG file to save." );
                nFC.type( Fl_Native_File_Chooser::BROWSE_SAVE_FILE );
                nFC.filter( "RAW Image\t*.raw\nIMG Image\t*.img" );
                nFC.directory( currentPath );
                nFC.preset_file(NULL);

                int retVal = nFC.show();

                const char* refstr = nFC.filename(0);
#ifdef _WIN32
                wstring refp;

                // Make conversion UTF8 to current locale ...
                if( fl_utf8test( refstr, strlen(refstr) ) >1 )
                {
                    wchar_t tmpwcs[256] = {0};

                    fl_utf8towc( refstr, strlen(refstr), tmpwcs, 256 );

                    refp = tmpwcs;
                }
                else
                {
                    refp = convertM2W( refstr );
                }
#else
				string refp = refstr;
#endif /// of _WIN32

                if ( refp.size() == 0 )
                {
                    grpOverlay->hide();
                    boxDspWait->label( DEF_STR_WAIT4CALC );
                    grpMain->activate();
                    reserveRedraw();

                    return NULL;
                }

#ifdef _WIN32
                if ( refp.find( L'.' ) == wstring::npos )
                {
                    refp += L".img";
                }
#else
                if ( refp.find( '.' ) == string::npos )
                {
                    refp += ".img";
                }
#endif /// of _WIN32
                // Make rescaled RAW ...

                RAWProcessor::RescaleType rtp = RAWProcessor::RESCALE_NEAREST;

                switch ( chsResizeType->value() )
                {
                    case 1 :
                        rtp = RAWProcessor::RESCALE_BILINEAR;
                        break;

                    case 2:
                        rtp = RAWProcessor::RESCALE_BICUBIC;
                        break;

                    case 3:
                        rtp = RAWProcessor::RESCALE_LANZCOS3;
                        break;
                }

                int new_w   = imgDspBox->imgw();
                int new_h   = imgDspBox->imgh();

                RAWProcessor* newrawproc = mediRAW->Rescale( new_w, new_h, rtp );

                if( newrawproc != NULL )
                {
                    // Need to transform .. (but don't know why )
                    //newrawproc->ApplyTransform( TRANSFORM_ROTATE | TRANSFORM_PARAM_ROT_C90 );
                    if ( userevered == false )
                    {
                        newrawproc->InvertAuto();
                    }

#ifdef _WIN32
                    bool fwritten = newrawproc->SaveToFile( (wchar_t*)refp.c_str() );
#else
                    bool fwritten = newrawproc->SaveToFile( refp.c_str() );
#endif /// of _WIN32

                    if ( fwritten == false )
                    {
#ifdef _WIN32
                        MessageBox( fl_xid( window ),
                                    TEXT("Save RAW image failure !\n"
                                         "Check file system access." ),
                                    TEXT("Save to RAW"),
                                    MB_ICONERROR );
#else
                    fl_message_title( "Save to RAW" );
                    fl_message_hotspot( 1 );
                    fl_alert( "Save RAW image failure !\nCheck file system access." );
#endif // _WIN32
                    }
                    else
                    {
#ifdef _WIN32
                        wstring infofn = refp;
                        infofn += L".xml";

                        if ( _waccess( infofn.c_str(), F_OK ) == 0 )
                        {
                            _wunlink( infofn.c_str() );
                        }

                        wstring xmlsrc = L"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";

                        xmlsrc += L"<file ";

                        wchar_t xmltmp[128] = {0};
                        wsprintf( xmltmp, L"name=\"%S\"/>\n", convertW2M( refp.c_str() ) );

                        xmlsrc += xmltmp;
                        xmlsrc += L"\t<sizes>\n";

                        wsprintf( xmltmp, L"\t\t<source width=\"%d\" height=\"%d\" />\n",
                                            mediRAW->Width(),
                                            mediRAW->Height() );

                        xmlsrc += xmltmp;

                        wsprintf( xmltmp, L"\t\t<scaled width=\"%d\" height=\"%d\" />\n",
                                            newrawproc->Width(),
                                            newrawproc->Height() );

                        xmlsrc += xmltmp;
                        xmlsrc += L"\t</sizes>\n";
                        xmlsrc += L"</file>\n";

                        FILE* infp = _wfopen( infofn.c_str(), L"w" );
                        if ( infp != NULL )
                        {
                            int   convutf8sz  = xmlsrc.size() * 2;
                            char* convutf8str = new char[ convutf8sz ];

                            int convsz = fl_utf8fromwc( convutf8str, convutf8sz,
                                                        xmlsrc.c_str(), xmlsrc.size() );

                            fwrite( convutf8str, 1, convsz, infp );
                            fclose( infp );

                            delete convutf8str;
                        }
#else
                        string infofn = refp;
                        infofn += ".xml";

                        if ( access( infofn.c_str(), F_OK ) == 0 )
                        {
                            unlink( infofn.c_str() );
                        }

                        string xmlsrc = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n";

                        xmlsrc += "<file ";

                        char xmltmp[128] = {0};
                        sprintf( xmltmp, "name=\"%s\"/>\n", refp.c_str() );

                        xmlsrc += xmltmp;
                        xmlsrc += "\t<sizes>\n";

                        sprintf( xmltmp, "\t\t<source width=\"%d\" height=\"%d\" />\n",
                                         mediRAW->Width(),
                                         mediRAW->Height() );

                        xmlsrc += xmltmp;

                        sprintf( xmltmp, "\t\t<scaled width=\"%d\" height=\"%d\" />\n",
                                         newrawproc->Width(),
                                         newrawproc->Height() );

                        xmlsrc += xmltmp;
                        xmlsrc += "\t</sizes>\n";
                        xmlsrc += "</file>\n";

                        FILE* infp = fopen( infofn.c_str(), "w" );
                        if ( infp != NULL )
                        {
                            fwrite( xmlsrc.c_str(), 1, xmlsrc.size(), infp );
                            fclose( infp );
                        }

#endif /// of _WIN32
#ifdef _WIN32
                        X_MessageBoxTimeout( fl_xid( window ),
                                             TEXT("Save RAW image file success.\n"
                                                  "(This dialog automatically close in 5sec."),
                                             TEXT("Save to RAW"),
                                             MB_ICONINFORMATION,
                                             5000 );
#else
                        fl_message_title( "Save to RAW" );
                        fl_message_hotspot( 1 );
                        fl_message( "Save RAW image file success." );
#endif // _WIN32

                    }

                    delete newrawproc;
                }

            }
            break;
    }

    Fl::lock();
    grpOverlay->hide();
    boxDspWait->label( DEF_STR_WAIT4CALC );
    grpMain->activate();
    grpMain->redraw();
    Fl::unlock();
    Fl::awake();

    reserveRedraw();

    return NULL;
}

void DialogMain::PreloadCB()
{
    Fl::remove_timeout(preload_cb, this);

    if ( needpreload == true )
    {
        needpreload = false;
        loadImage( preloadfilename.c_str() );
        preloadfilename.clear();
    }

    window->activate();
}

void  DialogMain::ProcRedrawTimer()
{
    if ( workingnow == true )
    {
        if ( proceedingredraw == true )
            proceedingredraw = false;

        return;
    }

    if ( proceedingredraw == true )
    {
        if ( window->visible_r() > 0 )
        {
            if ( window->active_r() > 0 )
            {
                window->redraw();
            }
        }

        proceedingredraw = false;
    }
}

void DialogMain::drawuserimage(int w, int h)
{
    // draw something to display on !
}

void DialogMain::drawHistogram()
{
    if ( mediRAW == NULL )
        return;

    if ( mediRAW->WeightsCount() == 0 )
        return;

    if ( boxHistogram == NULL )
        return;

    vector<unsigned int> histograph;

    mediRAW->GetWeights( &histograph );

    if( boxHistogram != NULL )
    {
        boxHistogram->data( histograph );

        if ( weight_report.timestamp > 0 )
        {
            boxHistogram->emphase( true );
            boxHistogram->emphase_value( weight_report.threshold_wide_min,
                                         weight_report.threshold_wide_max );
        }
        else
        {
            boxHistogram->emphase( false );
        }

        unsigned short m_l = mediRAW->MaximumLevel();
        boxHistogram->limit( m_l );
        boxHistogram->redraw();
    }

    histograph.clear();
}


void DialogMain::displayzoomratio()
{
    float f_v  = vsImageZoom->value();

    if ( f_v > 0.0f )
    {
        imgDspBox->multiplyratio( f_v );

        char imgszwstr[32] = {0};
        char imgszhstr[32] = {0};

        itoa( imgDspBox->imgw(), imgszwstr, 10 );
        itoa( imgDspBox->imgh(), imgszhstr, 10 );

        inpImageZoomWidth->value( imgszwstr );
        inpImageZoomHeight->value( imgszhstr );
    }
}

void DialogMain::updateFitButtonState( int seq )
{
    switch( seq )
    {
        case 0:
            if ( imgDspBox != NULL )
            {
                if ( imgDspBox->image() != NULL )
                {
                    imgDspBox->fitwidth();
                    vsImageZoom->value( imgDspBox->multiplyratio() );
                }
            }
            break;

        case 1:
            if ( imgDspBox != NULL )
            {
                if ( imgDspBox->image() != NULL )
                {
                    vsImageZoom->value( 1.0f );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;
    }

    displayzoomratio();
}

void DialogMain::clearLog()
{
    if ( bufImageInfo != NULL )
    {
        bufImageInfo->remove( 0, bufImageInfo->length() );
    }
}

void DialogMain::addLog( const char* t )
{
    static bool logonworking = false;

    if ( logonworking == true )
        return;

    logonworking = true;

    if ( ( bufImageInfo != NULL ) && ( logImageInfo != NULL ) )
    {
        int prevl = logImageInfo->count_lines(0, logImageInfo->buffer()->length(), 1);

        bufImageInfo->append( t );

        logImageInfo->insert_position( logImageInfo->buffer()->length() );

        bool redrawneed = false;
        int nextl = logImageInfo->count_lines(0, logImageInfo->buffer()->length(), 1);

        if ( prevl != nextl )
        {
            redrawneed = true;
            logImageInfo->scroll( logImageInfo->count_lines(0, logImageInfo->buffer()->length(), 1), 0 );
        }

        logImageInfo->redraw();
    }

    logonworking = false;
}

void DialogMain::reserveRedraw()
{
#ifdef __linux__
    Fl::wait();
#endif

    if ( proceedingredraw == false )
    {
        proceedingredraw = true;
#ifdef _WIN32
        window->redraw();

		if ( window != NULL )
        {
			InvalidateRect( fl_xid( window ), NULL, TRUE );
		}
#else
        Fl::repeat_timeout( 0.01f, fl_timer_cb, this );
#endif
    }
    else
    {
        // retry to call redraw timer in 100ms.
        Fl::repeat_timeout( 0.1f, fl_timer_cb, this );
    }
}

void DialogMain::loadConfig()
{
    string cfile  = currentPath;
           cfile += DEF_D_INI_FILE;

    if ( access( cfile.c_str(), F_OK ) != 0 )
    {
        preloadx = 20;
        preloady = 30;
        useinverted = true;
    }

    minIni* ini = new minIni( cfile );
    if ( ini != NULL )
    {
        preloadx        = ini->geti( "POSITION", "X", 20 );
        preloady        = ini->geti( "POSITION", "Y", 30 );
        preloadw        = ini->geti( "POSITION", "W", 0 );
        preloadh        = ini->geti( "POSITION", "H", 0 );

        preloadheight   = ini->geti( "DEFAULT", "HEIGHT", 3072 );
        rotatetype      = ini->geti( "DEFAULT", "ROTATE", 0 );

        useinverted     = ini->getbool( "RAWIMAGE", "INVERT", true );
        useswap         = ini->getbool( "RAWIMAGE", "SWAP", false );
        use8bit         = ini->getbool( "RAWIMAGE", "SINGLEBYTE", false );

        switch( rotatetype )
        {
            default:
            case 0:
                rotatemask = TRANSFORM_NONE;
                break;

            case 1:
                rotatemask = TRANSFORM_SWAP | TRANSFORM_PARAM_SWAP_H;
                break;

            case 2:
                rotatemask = TRANSFORM_SWAP | TRANSFORM_PARAM_SWAP_V;
                break;

            case 3:
                rotatemask = TRANSFORM_ROTATE | TRANSFORM_PARAM_ROT_C90;
                break;

            case 4:
                rotatemask = TRANSFORM_ROTATE | TRANSFORM_PARAM_ROT_C180;
                break;

            case 5:
                rotatemask = TRANSFORM_ROTATE | TRANSFORM_PARAM_ROT_C270;
                break;

            case 13:
                rotatemask = TRANSFORM_SWAP | TRANSFORM_PARAM_SWAP_H |
                             TRANSFORM_ROTATE | TRANSFORM_PARAM_ROT_C90;
                break;

            case 14:
                rotatemask = TRANSFORM_SWAP | TRANSFORM_PARAM_SWAP_H |
                             TRANSFORM_ROTATE | TRANSFORM_PARAM_ROT_C180;
                break;

            case 15:
                rotatemask = TRANSFORM_SWAP | TRANSFORM_PARAM_SWAP_H |
                             TRANSFORM_ROTATE | TRANSFORM_PARAM_ROT_C270;
                break;

            case 23:
                rotatemask = TRANSFORM_SWAP | TRANSFORM_PARAM_SWAP_V |
                             TRANSFORM_ROTATE | TRANSFORM_PARAM_ROT_C90;
                break;

            case 24:
                rotatemask = TRANSFORM_SWAP | TRANSFORM_PARAM_SWAP_V |
                             TRANSFORM_ROTATE | TRANSFORM_PARAM_ROT_C180;
                break;

            case 25:
                rotatemask = TRANSFORM_SWAP | TRANSFORM_PARAM_SWAP_V |
                             TRANSFORM_ROTATE | TRANSFORM_PARAM_ROT_C270;
                break;

        }

        string refstr;
        unsigned int tmpc = 0;

        refstr = ini->gets( "COLORS", "IV_MINIMAP" );
        if ( refstr.size() > 0 )
        {
            tmpc = strtoll( refstr.c_str(), NULL, 0 );
            ucol_enabled = true;
            ucol_iv_minimap = tmpc;
            tmpc = 0;
            refstr.clear();
        }
        else
        {
            ucol_enabled = false;
        }

        refstr = ini->gets( "COLORS", "HG_1" );
        if ( refstr.size() > 0 )
        {
            tmpc = strtoll( refstr.c_str(), NULL, 0 );
            ucol_enabled = true;
            ucol_hg_back = tmpc;
            tmpc = 0;
            refstr.clear();
        }
        else
        {
            ucol_enabled = false;
        }

        refstr = ini->gets( "COLORS", "HG_2" );
        if ( refstr.size() > 0 )
        {
            tmpc = strtoll( refstr.c_str(), NULL, 0 );
            ucol_enabled = true;
            ucol_hg_histo = tmpc;
            tmpc = 0;
            refstr.clear();
        }
        else
        {
            ucol_enabled = false;
        }


        refstr = ini->gets( "COLORS", "HG_3" );
        if ( refstr.size() > 0 )
        {
            tmpc = strtoll( refstr.c_str(), NULL, 0 );
            ucol_enabled = true;
            ucol_hg_sel = tmpc;
            tmpc = 0;
            refstr.clear();
        }
        else
        {
            ucol_enabled = false;
        }

        refstr = ini->gets( "COLORS", "HG_4" );
        if ( refstr.size() > 0 )
        {
            tmpc = strtoll( refstr.c_str(), NULL, 0 );
            ucol_enabled = true;
            ucol_hg_histosel = tmpc;
            tmpc = 0;
            refstr.clear();
        }
        else
        {
            ucol_enabled = false;
        }

        delete ini;
    }
}

void DialogMain::saveConfig()
{
    string cfile  = currentPath;
           cfile += DEF_D_INI_FILE;

    minIni* ini = new minIni( cfile );
    if ( ini != NULL )
    {
        ini->put( "POSITION", "X", window->x() );
        ini->put( "POSITION", "Y", window->y() );
        ini->put( "POSITION", "W", window->w() );
        ini->put( "POSITION", "H", window->h() );

        int imgHS = atoi( inpHSize->value() );
        if ( imgHS == 0 )
        {
            imgHS = DEFAULT_IMAGE_HEIGHT;
        }

        ini->put( "DEFAULT", "HEIGHT", imgHS );
        ini->put( "DEFAULT", "ROTATE", rotatetype );
        ini->put( "RAWIMAGE", "INVERT", useinverted );

        delete ini;
    }
}

bool DialogMain::load8bit( const char* ref, char* &newbuff, unsigned &newsz )
{
    if ( ref == NULL )
        return false;

    bool retb = false;

    FILE* fp = fopen( ref, "rb" );
    if ( fp != NULL )
    {
        fseek( fp, 0L, SEEK_END );
        unsigned fsz = ftell( fp );
        rewind( fp );

        if ( fsz > 0 )
        {
            char* tmpbuff = new char[ fsz ];
            if ( tmpbuff != NULL )
            {
                fread( tmpbuff, 1, fsz, fp );
                fclose( fp );

                newbuff = new char[ fsz * 2 ];
                if ( newbuff != NULL )
                {
                    for( unsigned cnt=0; cnt<fsz; cnt++ )
                    {
                        newbuff[ cnt*2 ] = tmpbuff[ cnt ];
                    }

                    newsz = fsz * 2;
                    retb = true;
                }

                delete[] tmpbuff;
            }
        }
    }

    return retb;
}

void DialogMain::loadImage( const char* ref )
{
    if ( nowloading == true )
        return;

    nowloading = true;

    int retVal = -1;
    string loadfile;

    if ( ref == NULL )
    {
        Fl_Native_File_Chooser nFC;

        nFC.title( "Choose a RAW/DCM image file to open" );
        nFC.type( Fl_Native_File_Chooser::BROWSE_FILE );
        nFC.filter( "RAW Image\t*.raw;*.img\nDICOM\t*.dcm" );
        nFC.directory( openPath );
        nFC.preset_file(NULL);

        retVal = nFC.show();

        if ( retVal == 0 )
        {
			if ( nFC.filename() == NULL )
				return;

            loadfile = nFC.filename();

            // test loadfile has DCM ext.
            if ( ( loadfile.rfind( ".dcm" ) != string::npos ) ||
                 ( loadfile.rfind( ".DCM" ) != string::npos ) )
            {
                nowloading = false;
                loadDCM( loadfile.c_str() );
                return;
            }
        }
    }
    else
    {
        loadfile = ref;

		// check prefix of "file://" , then remove it.
		const char filepref[] = "file://";
		::size_t fpos = loadfile.find( filepref );
		if ( fpos == 0 )
		{
			int prelen = loadfile.size();
			loadfile = loadfile.substr( strlen( filepref ) );
		}

        retVal = 0;
    }

    bool utf8encoded = false;

    // Make conversion UTF8 to current locale ...
    if( fl_utf8test( loadfile.c_str(), loadfile.size() ) >1 )
    {
        utf8encoded = true;
        string convstr = fl_utf2mbcs( loadfile.c_str() );
        loadfile = convstr;
    }

    if ( retVal == 0 )
    {
        if ( loadfile.size() == 0 )
        {
#ifdef _WIN32
            MessageBox( fl_xid( window ),
                        TEXT("No image selected."),
                        TEXT("Notice:"),
                        MB_ICONWARNING );
#else
            fl_message_title( "Notice:" );
            fl_message_hotspot( 1 );
            fl_alert( "No image selected" );
#endif // _WIN32
            nowloading = false;
            return;
        }

        if ( mediRAW == NULL )
        {
            mediRAW = new RAWProcessor();
        }

        if ( mediRAW != NULL )
        {
            if ( windowRawThumb != NULL )
            {
                windowRawThumb->hide();
            }

            fl_cursor( FL_CURSOR_WAIT, 0, 0 );

#if( UNICODE && DEBUG && _WIN32 )
            if ( sizeof(TCHAR) != 2 )
            {
                nowloading = false;
                MessageBox( fl_xid( window ),
                            TEXT("What's wrong? why it is not unicode ?"),
                            TEXT("Unicode Error:"),
                            MB_ICONERROR );
                return;
            }
#endif
            int imgHS = atoi( inpHSize->value() );
            if ( imgHS == 0 )
            {
                imgHS = DEFAULT_IMAGE_HEIGHT;
            }

            _TSTRING refPath =  _TCM2W( loadfile.c_str() );

            Perfmon perfmon;
            perfmon.SetMonStart();

            mediRAW->Swapping( useswap );

            bool bLoaded = false;

            if ( use8bit == false )
            {
                bLoaded = mediRAW->Load( refPath.c_str(), TRANSFORM_NONE, imgHS );
            }
            else
            {
                #ifdef DEBUG
                printf( "8bit loading ...\n");
                #endif // DEBUG
                char* convbuff = NULL;
                unsigned convbuffsz = 0;

                if( load8bit( ref, convbuff, convbuffsz ) == true )
                {
                    bLoaded = mediRAW->LoadFromMemory( convbuff, convbuffsz,
                                                       TRANSFORM_NONE, imgHS );
                    delete[] convbuff;
                }
            }

            if ( bLoaded == true )
            {
                clearLog();

                perfmon.SetMonStop();
                unsigned long mon1 = perfmon.GetPerfMS();
                char tmpperfstr[128] = {0};

                sprintf( tmpperfstr, "File loading time took %ld ms.\n\n", mon1 );
                addLog( tmpperfstr );

                if ( utf8encoded == true )
                {
                    rawimagefilename = ref;
                }
                else
                {
                    rawimagefilename = loadfile;
                }

                window->label( rawimagefilename.c_str() );
            }
            else
            {
                //fl_alert("Failed to load RAW image:%s\n", loadfile.c_str() );
#ifdef _WIN32
                TCHAR tmperrstr[512] = {0};
                _stprintf( tmperrstr,
                           TEXT("Failed to load file:%s\n"),
                           _TCW2M( loadfile.c_str() ) );
                MessageBox( fl_xid( window ),
                            tmperrstr,
                            TEXT("Load Failure:"),
                            MB_ICONWARNING );
#else
                char tmperrstr[512] = {0};
                sprintf( tmperrstr,
                           "Failed to load file:%s\n",
                           loadfile.c_str() );

                fl_message_title( "Load Failure:" );
                fl_message_hotspot( 1 );
                fl_alert( "%s", tmperrstr );
#endif // _WIN32
            }

            if ( mediRAW->GetAnalysisReport( weight_report ) == true )
            {
                weight_report.threshold_wide_min = 0;
            }
            else
            {
                memset( &weight_report, 0, sizeof( RAWProcessor::WeightAnalysisReport ) );
#ifdef _WIN32
                weight_report.timestamp = GetTickCount();
#else
                weight_report.timestamp = 1;
#endif /// of _WIN32
                weight_report.threshold_wide_min = mediRAW->MinimumLevel();
                weight_report.threshold_wide_max = mediRAW->MaximumLevel();
            }

            userzoom = false;
            if ( useinverted == true )
            {
                userevered = true;
            }

            if ( cbUseThreshold != NULL )
            {
                if ( cbUseThreshold->value() > 0 )
                {
                    cbUseThreshold->value( 0 );
                }
            }

            updateImageInfo();
            drawHistogram();
            importRAWimage();
            fl_cursor( FL_CURSOR_DEFAULT, 0, 0 );

            window->label( rawimagefilename.c_str() );
        }
    }

    nowloading = false;
    loadtypeDCM = false;
}

void DialogMain::loadDCM( const char* ref )
{
	if ( ref == NULL )
		return;

    if ( nowloading == true )
        return;

    if ( ref != NULL )
    {
        bool   utf8encoded = false;
        string strref = ref;

        if( fl_utf8test( ref, strlen( ref ) ) >1 )
        {
            utf8encoded = true;
            string convstr = fl_utf2mbcs( ref );
            strref = convstr;
        }

        // check prefix of "file://" , then remove it.
		const char filepref[] = "file://";
		::size_t fpos = strref.find( filepref );
		if ( fpos == 0 )
		{
			strref = strref.substr( strlen( filepref ) );
		}


        //wchar_t* wref = convertM2W( ref );
        wstring wref = convertM2W( strref.c_str() );

        if ( wref.size() > 0 )
        {
            if ( mediRAW == NULL )
            {
                mediRAW = new RAWProcessor();
            }

            if ( mediRAW == NULL )
            {
                nowloading = false;
                return;
            }

            fl_cursor( FL_CURSOR_WAIT, 0, 0 );

            if ( OpenDCMW( wref.c_str() ) == true )
            {
                clearLog();

                int elemdcms = GetElementCount();
                if ( elemdcms > 0 )
                {
                    // Find some elements ...
                    ImageInformation dcmimg = {0};

                    if ( ReadPixelData( &dcmimg ) == true )
                    {
#ifdef DEBUG
                        printf( "DICOM image res = %d x %d x %dbit\n",
                                dcmimg.width, dcmimg.height, dcmimg.bpp );
#endif // DEBUG
                        int bytes = 0;

                        if ( dcmimg.bpp > 32 )
                        {
                            bytes = 8;
                        }
                        else
                        if ( dcmimg.bpp > 16 )
                        {
                            bytes = 4;
                        }
                        else
                        if ( dcmimg.bpp > 8 )
                        {
                            bytes = 2;
                        }
                        else
                        {
                            bytes = 1;
                        }

                        static char newhszstr[32] = {0};
                        Perfmon perfmon;
                        perfmon.SetMonStart();

                        mediRAW->Unload();
                        imgDspBox->unloadimage();

                        unsigned long newbuffsz = dcmimg.width * dcmimg.height * bytes;
#ifdef DEBUG
                        printf("DCM load to memory: %dx%dx%d , %ld bytes\n",
                                dcmimg.width, dcmimg.height, bytes, newbuffsz );
#endif // DEBUG

                        mediRAW->LoadFromMemory( (char*)dcmimg.pixels,
                                                 newbuffsz,
                                                 TRANSFORM_NONE,
                                                 dcmimg.width );

                        perfmon.SetMonStop();

                        loadtypeDCM = true;
                        userevered = false;

                        // Check reversed, 2050:0020 Presentation LUT shape !
                        DCMTagElement* presLUTs = FindElement( 0x20500020 );
                        if ( presLUTs != NULL )
                        {
                            if ( strstr( presLUTs->staticbuffer, "INVERSE" ) != NULL )
                            {
                                //mediRAW->ReverseAuto();
                                userevered = true;
                            }
                        }

                        unsigned long long mon1 = perfmon.GetPerfMS();
                        char tmpperfstr[128] = {0};

                        sprintf( tmpperfstr, "Load DCM time took %llu ms.\n\n", mon1 );
                        addLog( tmpperfstr );

                        sprintf( newhszstr, "%d", dcmimg.width );
                        inpHSize->value( newhszstr );

                        if ( mediRAW->GetAnalysisReport( weight_report ) == true )
                        {
                            weight_report.threshold_wide_min = 0;
                        }
                        else
                        {
                            memset( &weight_report, 0, sizeof( RAWProcessor::WeightAnalysisReport ) );
#ifdef _WIN32
                            weight_report.timestamp = GetTickCount();
#else
                            weight_report.timestamp = 1;
#endif /// of _WIN32
                            weight_report.threshold_wide_min = mediRAW->MinimumLevel();
                            weight_report.threshold_wide_max = mediRAW->MaximumLevel();
                        }

                        DCMTagElement* dcmtag = NULL;

                        bool     doingthreshold = true;
                        unsigned w_center = 0;

                        dcmtag = FindElement( 0x00281050 ); /// window center ....
                        if ( dcmtag != NULL )
                        {
                            w_center = atoi( dcmtag->staticbuffer );
                        }

                        dcmtag = FindElement( 0x00281051 ); /// window width ...

                        if ( dcmtag != NULL )
                        {
                            unsigned w_width = atoi( dcmtag->staticbuffer );
                            if ( ( w_width > 0 ) && ( w_center == 0 ) )
                            {
                                w_center = w_width / 2;
                            }

                            v_thld_max = w_center + w_width / 2;
                            v_thld_min = w_center - w_width / 2;

                            if ( v_thld_min < 0 )
                            {
                                v_thld_min = 0;
                            }

                            if ( v_thld_max <= v_thld_min )
                            {
                                v_thld_max = mediRAW->MaximumLevel();
                            }
                        }
                        else
                        {
                            v_thld_min = 0;
                            v_thld_max = mediRAW->MaximumLevel();
                        }

                        weight_report.threshold_wide_min = v_thld_min;
                        weight_report.threshold_wide_max = v_thld_max;
                        boxHistogram->emphase_value( v_thld_min, v_thld_max );

                        userzoom = false;

                        updateImageInfo();
                        drawHistogram();

                        if ( doingthreshold == true )
                        {
                            if ( cbUseThreshold != NULL )
                            {
                                cbUseThreshold->value( 1 );
                                thresholdControlModify( 1 );
                            }
                        }
                        else
                        {
                            importRAWimage();
                        }

                        if ( utf8encoded == true )
                        {
                            rawimagefilename = ref;
                        }
                        else
                        {
                            rawimagefilename = strref;
                        }

                        window->label( rawimagefilename.c_str() );
                    }
                }

                CloseDCM();

                fl_cursor( FL_CURSOR_DEFAULT, 0, 0 );

                window->label( rawimagefilename.c_str() );

            }
        }
    }

    nowloading = false;
}

void DialogMain::updateImageInfo()
{
    if ( logImageInfo == NULL )
        return;

    if ( mediRAW == NULL )
    {
        addLog("*ERROR*\nImage processor not loaded.\n");
        return;
    }

    if ( mediRAW->PixelCount() == 0 )
    {
        addLog("*ERROR*\nImage not loaded. size = 0\n");
        return;
    }

    static char infoText[1024] = {0};

    sprintf( infoText,
             "Image information update:\n"
             "Pixels count = %d ( %d x %d )\nImage levels, min = %d | med = %d | max = %d\n",
             mediRAW->PixelCount(),
             mediRAW->Width(),
             mediRAW->Height(),
             mediRAW->MinimumLevel(),
             mediRAW->MediumLevel(),
             mediRAW->MaximumLevel());

    addLog( infoText );

    // analyser
    if ( weight_report.timestamp > 0 )
    {
        sprintf( infoText,
                 "%s\nThreshold min = %d, max = %d, wide = %d\n\n",
                 infoText,
                 weight_report.threshold_wide_min,
                 weight_report.threshold_wide_max,
                 weight_report.threshold_wide_max - weight_report.threshold_wide_min );
        addLog( infoText );
    }
}

void DialogMain::importRAWimage( bool useThreshold )
{
    if ( mediRAW != NULL )
    {
        Perfmon perfmon;
        perfmon.SetMonStart();

        vector<unsigned char> downscaled;

        if ( useThreshold == true )
        {
            mediRAW->Get8bitThresholdedImage( weight_report, &downscaled, userevered );
        }
        else
        {
            mediRAW->Get8bitDownscaled( &downscaled , RAWProcessor::DNSCALE_FULLDOWN, userevered );
        }

        imgDspBox->clear_damage();

        if ( downscaled_raw_buffer != NULL )
        {
            delete[] downscaled_raw_buffer;
            downscaled_raw_buffer = NULL;
        }

        int imgSize = downscaled.size();
        if ( imgSize > 0 )
        {
            downscaled_raw_buffer = new unsigned char[ imgSize * 3 ];

            for( int cnt=0; cnt<imgSize; cnt++ )
            {
                unsigned char* refPixel = &downscaled_raw_buffer[cnt * 3];

                refPixel[0] = downscaled[cnt];
                refPixel[1] = downscaled[cnt];
                refPixel[2] = downscaled[cnt];
            }

            int imgw = mediRAW->Width();
            int imgh = mediRAW->Height();

            if ( ( imgw > 0 ) && ( imgh > 0 ) )
            {
                Fl_RGB_Image* newImg = new Fl_RGB_Image( downscaled_raw_buffer, imgw, imgh, 3);

                imgDspBox->image( newImg );
            }

            perfmon.SetMonStop();
            unsigned long mon1 = perfmon.GetPerfMS();
            char tmpperfstr[128] = {0};

            sprintf( tmpperfstr, "## Image process time took %ld ms.\n", mon1 );
            addLog( tmpperfstr );

            ProcWidgetCB( vsImageZoom );

        }

        downscaled.clear();
    }
}

void DialogMain::thresholdControl()
{
    if ( ( mediRAW == NULL ) || ( imgDspBox->image() == NULL ) )
    {
        if ( cbUseThreshold->active_r() == 0 )
        {
            cbUseThreshold->activate();
        }

        return;
    }

    bool thresholed = false;
    int checkedstate = cbUseThreshold->value();

    if ( checkedstate > 0 )
    {
        if ( weight_report.timestamp > 0 )
        {
            thresholdControlModify(0);
            thresholed = true;
        }
    }

    if( thresholed == false )
    {
        if ( grpMain->active_r() > 0)
        {
            fl_cursor( FL_CURSOR_WAIT, 0, 0 );

            grpMain->deactivate();
            grpOverlay->show();
            window->redraw();
            //reserveRedraw();
#ifdef _WIN32
            Sleep( 10 );
#endif /// of _WIN32

            importRAWimage();

            grpOverlay->hide();
            grpMain->activate();

            cbUseThreshold->activate();

            reserveRedraw();
#ifdef _WIN32
            Sleep( 10 );
#endif /// of _WIN32

            fl_cursor( FL_CURSOR_DEFAULT, 0, 0 );
        }
    }

}

void DialogMain::thresholdControlModify( int ccnt )
{
#ifdef DEBUG
    printf("[B]void DialogMain::thresholdControlModify( %d )\n", ccnt );
#endif // DEBUG
    int checkedstate = cbUseThreshold->value();

    if ( checkedstate > 0 )
    {
        if ( weight_report.timestamp > 0 )
        {
            updateImageInfo();
            importRAWimage( true );

            if ( cbUseThreshold->active_r() == 0 )
            {
                cbUseThreshold->activate();
            }
        }
    }
    else
    {
        reserveRedraw();
    }
#ifdef DEBUG
    printf("[E]void DialogMain::thresholdControlModify()\n" );
#endif // DEBUG
}

void DialogMain::heightApply()
{
    if ( mediRAW == NULL )
        return;

    const char* refstr = inpHSize->value();
    if ( strlen( refstr ) > 0 )
    {
        int hsize = atoi( refstr );
        if ( hsize > 0 )
        {
            fl_cursor( FL_CURSOR_WAIT, 0, 0 );

            grpMain->deactivate();

            mediRAW->ChangeHeight( hsize );
            updateImageInfo();

            bool uTrhld = false;

            if ( cbUseThreshold->value() > 0 )
            {
                uTrhld = true;
            }

            importRAWimage( uTrhld );

            fl_cursor( FL_CURSOR_DEFAULT, 0, 0 );
            grpMain->activate();
            reserveRedraw();
        }
    }
}

void DialogMain::createComponents()
{
    Fl::scrollbar_size( 10 );

    grpMain = new Fl_Group( COORD_GRPMAIN );
    GROUP_BEGIN( grpMain );

    Fl_Group* grpDivMain = new Fl_Group( COORD_GRPDIVMAIN );
    GROUP_BEGIN( grpDivMain );

        imgDspBox = new Fl_ImageViewer( COORD_IMGDSPBOX );
        if ( imgDspBox != NULL )
        {
            imgDspBox->color( fl_darker( COL_WINDOW ) );
            imgDspBox->labelcolor( COL_TEXT );
            imgDspBox->notifier( this );

            if ( ucol_enabled == true )
            {
                imgDspBox->minimapcolor( ucol_iv_minimap );
            }
        }

    GROUP_END( grpDivMain );

    printf( "---- group 1 done ----\n" );
    fflush( stdout );

    Fl_Group* grpDivTools = new Fl_Group(COORD_GRPDIVTOOL);
    GROUP_BEGIN( grpDivTools );

        Fl_Group* grpDivToolSec00 = new Fl_Group( COORD_GRPDIVSEC00 );
        GROUP_BEGIN( grpDivToolSec00 );

        Fl_Group* grpDivToolSec01 = new Fl_Group( COORD_GRPDIVSEC01 );
        GROUP_BEGIN( grpDivToolSec01 );

            if ( grpDivToolSec01 != NULL )
            {
                grpDivToolSec01->box( FL_FLAT_BOX );
                grpDivToolSec01->color( COL_WINDOW );
            }

            cbUseThreshold = new Fl_Light_Button( COORD_CHKBX_USERS, " Threshold");
            if ( cbUseThreshold != NULL )
            {
                cbUseThreshold->box( FL_THIN_UP_BOX );
                cbUseThreshold->color( COL_WINDOW );
                cbUseThreshold->labelcolor( COL_TEXT );
                cbUseThreshold->labelsize( DEF_FONT_SIZE );
                cbUseThreshold->clear_visible_focus();
                cbUseThreshold->tooltip( "Enable/Disable threshold render with below histogram." );
                cbUseThreshold->callback( fl_widget_cb, this );
            }

        GROUP_END( grpDivToolSec01 );

        Fl_Group* grpDivToolSec02 = new Fl_Group( COORD_GRPDIVSEC02 );
        GROUP_BEGIN( grpDivToolSec02 );

            if ( grpDivToolSec02 != NULL )
            {
                grpDivToolSec02->box( FL_FLAT_BOX );
                grpDivToolSec02->color( COL_WINDOW );
                grpDivToolSec02->clip_children( 1 );
            }

            vsImageZoom = new Fl_Spinner( COORD_IMGZOOM, "Zoom :" );
            if ( vsImageZoom != NULL )
            {
                vsImageZoom->type( FL_FLOAT_INPUT );
                vsImageZoom->box( FL_THIN_DOWN_BOX );
                vsImageZoom->up_button()->box( FL_FLAT_BOX );
                vsImageZoom->up_button()->labelcolor( FL_WHITE );
                vsImageZoom->up_button()->clear_visible_focus();
                vsImageZoom->down_button()->box( FL_FLAT_BOX );
                vsImageZoom->down_button()->labelcolor( FL_WHITE );
                vsImageZoom->down_button()->clear_visible_focus();
                vsImageZoom->color( COL_WINDOW );
                vsImageZoom->align( FL_ALIGN_LEFT );
                vsImageZoom->labelcolor( COL_TEXT );
                vsImageZoom->textcolor( COL_TEXT );
                vsImageZoom->labelsize( DEF_FONT_SIZE );
                vsImageZoom->textsize( DEF_FONT_SIZE );
                vsImageZoom->format( "%3.2f" );
                vsImageZoom->range( 0.0f, 5.1f );
                vsImageZoom->step( 0.05f );
                vsImageZoom->value( 1.0f );
                vsImageZoom->tooltip( "Zoom level, 10% to 500% (0.1 to 5.0)." );
                vsImageZoom->when( FL_WHEN_RELEASE );
                vsImageZoom->clear_visible_focus();
                vsImageZoom->callback( fl_widget_cb, this );
            }

            inpImageZoomWidth = new Fl_Int_Input( 270,410,50,20, "Width x Height :" );
            if ( inpImageZoomWidth != NULL )
            {
                inpImageZoomWidth->box( FL_THIN_DOWN_BOX );
                inpImageZoomWidth->color( COL_INPUT );
                inpImageZoomWidth->labelcolor( COL_TEXT );
                inpImageZoomWidth->textcolor( COL_TEXT );
                inpImageZoomWidth->labelsize( DEF_FONT_SIZE );
                inpImageZoomWidth->textsize( DEF_FONT_SIZE );
                inpImageZoomWidth->value( "0" );
                inpImageZoomWidth->callback( fl_widget_cb, this );
            }

            inpImageZoomHeight = new Fl_Int_Input( 340,410,50,20, "x " );
            if ( inpImageZoomHeight != NULL )
            {
                inpImageZoomHeight->box( FL_THIN_DOWN_BOX );
                inpImageZoomHeight->color( COL_INPUT );
                inpImageZoomHeight->labelcolor( COL_TEXT );
                inpImageZoomHeight->textcolor( COL_TEXT );
                inpImageZoomHeight->labelsize( DEF_FONT_SIZE );
                inpImageZoomHeight->textsize( DEF_FONT_SIZE );
                inpImageZoomHeight->value( "0" );
                inpImageZoomHeight->callback( fl_widget_cb, this );
            }


        GROUP_END( grpDivToolSec02 );

        Fl_Group* grpDivToolSec03 = new Fl_Group( COORD_GRPDIVSEC03 );
        if ( grpDivToolSec03 != NULL )
        {
            // Empty, resizabled group.
            grpDivToolSec03->box( FL_FLAT_BOX );
            grpDivToolSec03->color( COL_WINDOW );
            grpDivToolSec03->end();
        }

        Fl_Group* grpDivToolSec04 = new Fl_Group( COORD_GRPDIVSEC04 );
        GROUP_BEGIN( grpDivToolSec04 );

            if ( grpDivToolSec04 != NULL )
            {
                grpDivToolSec04->box( FL_FLAT_BOX );
                grpDivToolSec04->color( COL_WINDOW );
            }

            btnFitToWidth = new Fl_Button( COORD_BTNFIT2WID, "@<->");
            if ( btnFitToWidth != NULL )
            {
                btnFitToWidth->box( FL_THIN_UP_BOX );
                btnFitToWidth->color( COL_WINDOW );
                btnFitToWidth->labelcolor( COL_TEXT );
                btnFitToWidth->labelsize( DEF_FONT_SIZE + 1 );
                btnFitToWidth->tooltip( "Fit image width to current view." );
                btnFitToWidth->clear_visible_focus();
                btnFitToWidth->callback( fl_widget_cb, this );
            }

            btnFitToOrigin = new Fl_Button( COORD_BTN1TO1, "@square");
            if ( btnFitToOrigin != NULL )
            {
                btnFitToOrigin->box( FL_THIN_UP_BOX );
                btnFitToOrigin->color( COL_WINDOW );
                btnFitToOrigin->labelcolor( COL_TEXT );
                btnFitToOrigin->labelsize( DEF_FONT_SIZE );
                btnFitToOrigin->tooltip( "Fit image to 1:1 original scale." );
                btnFitToOrigin->clear_visible_focus();
                btnFitToOrigin->callback( fl_widget_cb, this );
            }

            chsResizeType = new Fl_Choice( COORD_CHSRSTYPE, "Resize Method:" );
            if ( chsResizeType != NULL )
            {
                chsResizeType->box( FL_THIN_DOWN_BOX );
                chsResizeType->color( COL_WINDOW );
                chsResizeType->labelcolor( COL_TEXT );
                chsResizeType->labelsize( DEF_FONT_SIZE );
                chsResizeType->textcolor( COL_TEXT );
                chsResizeType->textsize( DEF_FONT_SIZE - 2 );
                chsResizeType->add("Nearest");
                chsResizeType->add("BiLinear");
                chsResizeType->add("BiCubic");
                chsResizeType->add("Lanzcos3");
                chsResizeType->value(0);
                chsResizeType->when( FL_WHEN_CHANGED );
                chsResizeType->tooltip( "Select resize method." );
                chsResizeType->clear_visible_focus();
                chsResizeType->callback( fl_widget_cb, this );
            }

        GROUP_END( grpDivToolSec04 );
        GROUP_END( grpDivToolSec00 );
        GROUP_RESIZABLE( grpDivToolSec00, grpDivToolSec03 );

        Fl_Group* grpDivToolSec10 = new Fl_Group( COORD_GRPDIVSEC10 );
        GROUP_BEGIN( grpDivToolSec10 );

            boxHistogram = new Fl_HistogramDrawer( COORD_BOX_HISTO );
            if ( boxHistogram != NULL )
            {
                boxHistogram->labelsize( DEF_FONT_SIZE );
                boxHistogram->labelcolor( FL_WHITE );
                boxHistogram->tooltip( "Adjusting min/max level with mouse left/right click." );
                boxHistogram->callback( fl_widget_cb, this );

                if ( ucol_enabled == true )
                {
                    boxHistogram->color( ucol_hg_back );
                    boxHistogram->color_histogram( ucol_hg_histo );
                    boxHistogram->color_selecthistogram( ucol_hg_histosel );
                    boxHistogram->color_selection( ucol_hg_sel );
                }
            }

        GROUP_END( grpDivToolSec10 );

        Fl_Group* grpDivToolSec20 = new Fl_Group( COORD_GRPDIVSEC20 );
        GROUP_BEGIN( grpDivToolSec20 );

        Fl_Group* grpDivToolSec21 = new Fl_Group( COORD_GRPDIVSEC21 );
        GROUP_BEGIN( grpDivToolSec21 );

            inpWSize = new Fl_Input( COORD_IMGWSIZE, "Pixel size (W x H):" );
            if ( inpWSize != NULL )
            {
                inpWSize->box( FL_THIN_DOWN_BOX );
                inpWSize->color( 0 );
                inpWSize->labelcolor( COL_INPUT );
                inpWSize->labelsize( DEF_FONT_SIZE );
                inpWSize->textcolor( COL_TEXT );
                inpWSize->textsize( DEF_FONT_SIZE );
                inpWSize->maximum_size( 1000 );
                inpWSize->value( "--auto--" );
                inpWSize->deactivate();
            }

            inpHSize = new Fl_Input( COORD_IMGHSIZE, "x " );
            if ( inpHSize != NULL )
            {
                inpHSize->box( FL_THIN_DOWN_BOX );
                inpHSize->color( COL_INPUT );
                inpHSize->labelcolor( COL_TEXT );
                inpHSize->labelsize( DEF_FONT_SIZE );
                inpHSize->textcolor( COL_TEXT );
                inpHSize->textsize( DEF_FONT_SIZE );
                inpHSize->maximum_size( 5 );
                inpHSize->cursor_color( COL_CURSOR );
                inpHSize->tooltip( "Write RAW image height size here, DICOM will automatically sens." );
                inpHSize->when( FL_WHEN_ENTER_KEY_ALWAYS );
                inpHSize->callback( fl_widget_cb, this );
                inpHSize->value( "3072" );
            }

        GROUP_END( grpDivToolSec21 );

    printf( "---- group 2 done ----\n" );
    fflush( stdout );

        Fl_Group* grpDivToolSec22 = new Fl_Group( COORD_GRPDIVSEC22 );
        GROUP_BEGIN( grpDivToolSec22 );

            Fl_Box* boxNobody = new Fl_Box( COORD_GRPDIVSEC22 );
            if ( boxNobody != NULL )
            {
                boxNobody->box( FL_NO_BOX );
                boxNobody->color( COL_WINDOW );
                boxNobody->labelcolor( COL_TEXT );
                boxNobody->tooltip( "(C)~2016 Rageworx software, Copyrighted by Raphael Kim (rageworx@@gmail.com)" );
                boxNobody->labelsize( DEF_FONT_SIZE );
            }

        GROUP_END( grpDivToolSec22 );

        Fl_Group* grpDivToolSec23 = new Fl_Group( COORD_GRPDIVSEC23 );
        GROUP_BEGIN( grpDivToolSec23 );

            btnLoadImage = new Fl_Button( COORD_BTNLOADIMG, "Load File" );
            if ( btnLoadImage != NULL )
            {
                btnLoadImage->box( FL_THIN_UP_BOX );
                btnLoadImage->color( COL_WINDOW );
                btnLoadImage->labelcolor( COL_TEXT );
                btnLoadImage->labelsize( DEF_FONT_SIZE );
                btnLoadImage->tooltip( "Loads RAW/IMG file from your storage.\n"
                                       "Or Drag and drop file to viewing area at above." );
                btnLoadImage->callback( fl_widget_cb, this );
            }

            btnSaveImage = new Fl_Menu_Button( COORD_BTNSAVEIMG, "Export image" );
            if ( btnSaveImage != NULL )
            {
                menuSaveImage[0].user_data(this);
                menuSaveImage[1].user_data(this);

                btnSaveImage->box( FL_THIN_UP_BOX );
                btnSaveImage->color( COL_WINDOW );
                btnSaveImage->labelcolor( COL_TEXT );
                btnSaveImage->labelsize( DEF_FONT_SIZE );
                btnSaveImage->menu( menuSaveImage );
                btnSaveImage->tooltip( "Export loaded image to PNG (8bit gray scale) file or RAW image."
                                       "PNG will applied current viewer state as like histogram threshold." );
            }

        GROUP_END( grpDivToolSec23 );
        GROUP_END( grpDivToolSec20 );
        GROUP_RESIZABLE( grpDivToolSec20, grpDivToolSec22 );

    GROUP_END( grpDivTools );
    GROUP_END( grpMain );

    if ( ( grpMain != NULL ) && ( grpDivMain != NULL ) )
    {
        grpMain->resizable( grpDivMain );
    }

    grpOverlay = new Fl_Group( COORD_GRPOVERLAY );
    GROUP_BEGIN( grpOverlay );

        boxDspWait = new Fl_TransBox( COORD_GRPOVERLAY, DEF_STR_WAIT4CALC);
        if ( boxDspWait != NULL )
        {
            //boxDspWait->color( 0x00000000 );
            boxDspWait->labelcolor( COL_TEXT );
            boxDspWait->labelsize( DEF_FONT_SIZE + 10 );
        }

    GROUP_END( grpOverlay );

    printf( "---- group O done ----\n" );
    fflush( stdout );


    if ( grpOverlay != NULL )
    {
        grpOverlay->hide();
    }
}

void DialogMain::createSubCompoents()
{
    windowLog = new Fl_Double_Window( 600,400, "Log:" );
    if ( windowLog != NULL )
    {
        windowLog->color( COL_WINDOW );
        windowLog->begin();

        logImageInfo = new Fl_Text_Display( 0, 0, windowLog->w(), windowLog->h() );
        if ( logImageInfo != NULL )
        {
            logImageInfo->box( FL_THIN_DOWN_BOX );
            logImageInfo->color( COL_LOG );
            logImageInfo->textfont( FL_COURIER );
            logImageInfo->textcolor( COL_LOG_TEXT );
            logImageInfo->textsize( DEF_FONT_SIZE );
            logImageInfo->scrollbar_box( FL_THIN_UP_BOX );
            logImageInfo->scrollbar_color( COL_WINDOW );
            logImageInfo->wrap_mode( Fl_Text_Display::WRAP_AT_BOUNDS, 2 );

            bufImageInfo = new Fl_Text_Buffer();
            if ( bufImageInfo != NULL )
            {
                logImageInfo->buffer( bufImageInfo );
            }
        }

        windowLog->end();
        windowLog->size_range( windowLog->w(), windowLog->h() );
        windowLog->resizable( logImageInfo );

        //windowLog->show();
#ifdef _WIN32
        SendMessage( fl_xid( windowLog ),
                     WM_SETICON,
                     ICON_BIG,
                     (LPARAM)hIconWindowLarge );

        SendMessage( fl_xid( windowLog ),
                     WM_SETICON,
                     ICON_SMALL,
                     (LPARAM)hIconWindowSmall );
#endif /// of _WIN32
    }

    windowRawThumb = new Fl_Double_Window( 500,200 );
    if ( windowRawThumb != NULL )
    {
        windowRawThumb->box( FL_THIN_UP_BOX );
        windowRawThumb->color( COL_WINDOW );
        windowRawThumb->clear_border();
        windowRawThumb->begin();

        int cw = windowRawThumb->w() / WRAW_THUMB_ARRAY_W;
        int ch = ( windowRawThumb->h() - 20 ) / WRAW_THUMB_ARRAY_H;
        int cx = 0;
        int cy = 0;
        int gx = ( windowRawThumb->w() - ( cw * WRAW_THUMB_ARRAY_W ) ) / 2;
        int gy = ( ( windowRawThumb->h() - 20 )- ( ch * WRAW_THUMB_ARRAY_H ) ) / 2;

        for( cy=0;cy<WRAW_THUMB_ARRAY_H;cy++)
        {
            for( cx=0;cx<WRAW_THUMB_ARRAY_W;cx++)
            {
                int curidx = cy*WRAW_THUMB_ARRAY_W+cx;
                boxRawThumbs[ curidx ] = new Fl_Box( cx * cw + gx,
                                                     cy * ch + gy,
                                                     cw, ch, "0" );
                if ( boxRawThumbs[ curidx ] != NULL )
                {
                    boxRawThumbs[ curidx ]->box( FL_FLAT_BOX );
                    boxRawThumbs[ curidx ]->align( FL_ALIGN_INSIDE | FL_ALIGN_CENTER  );
                    boxRawThumbs[ curidx ]->color( COL_LOG );
                    boxRawThumbs[ curidx ]->labelcolor( FL_GRAY );
                    boxRawThumbs[ curidx ]->labelsize( 8 );
                }
            }
        }

        boxRawThumbs[ WRAW_THUMB_ARRAY_SZ / 2 ]->box( FL_THIN_DOWN_BOX );

        cy = WRAW_THUMB_ARRAY_H * ch + 8;

        btnCloseRawThumb = new Fl_Button( 5, cy, 18, 18, "X" );
        if ( btnCloseRawThumb != NULL )
        {
            btnCloseRawThumb->box( FL_THIN_UP_BOX );
            btnCloseRawThumb->color( COL_WINDOW );
            btnCloseRawThumb->labelcolor( COL_TEXT );
            btnCloseRawThumb->labelsize( DEF_FONT_SIZE );
            btnCloseRawThumb->clear_visible_focus();
            btnCloseRawThumb->callback( fl_widget_cb, this );
        }

        boxRawThumbInfo = new Fl_Box( 30, cy,
                                      windowRawThumb->w() - 35, 18,
                                     "no info" );
        if ( boxRawThumbInfo != NULL )
        {
            boxRawThumbInfo->box( FL_THIN_DOWN_BOX );
            boxRawThumbInfo->color( COL_LOG );
            boxRawThumbInfo->align( FL_ALIGN_CLIP );
            boxRawThumbInfo->labelcolor( COL_LOG_TEXT );
            boxRawThumbInfo->labelsize( DEF_FONT_SIZE );
            boxRawThumbInfo->callback( fl_widget_cb, this );
        }

        windowRawThumb->end();
        windowRawThumb->callback( fl_widget_cb, this );
        windowRawThumb->set_modal();
    }
}

void DialogMain::OnRightClick( int x, int y)
{
    static char     outText[80] = {0};
    unsigned short  curRawPixel = 0;

    if ( ( windowRawThumb != NULL ) &&
         ( mediRAW != NULL ) &&
         ( mediRAW->Loaded() == true ) )
    {
        // fill RAW pixels in matrix
        int idx_div = ( WRAW_THUMB_ARRAY_W * WRAW_THUMB_ARRAY_H ) / 2;
        int raw_wid = mediRAW->Width();
        int raw_hei = mediRAW->Height();
        int raw_max = mediRAW->PixelCount();

        unsigned short max_lval = mediRAW->MaximumLevel();

        const unsigned short* ref_array = mediRAW->data();
        char tmp_buf[32] = {0};

        unsigned short minLevel = 65535;
        unsigned short maxLevel = 0;
        unsigned short levelDiff = 0;

        for( int cy=0; cy<WRAW_THUMB_ARRAY_H; cy++ )
        {
            for( int cx=0; cx<WRAW_THUMB_ARRAY_W; cx++ )
            {
                int mat_idx = cy*WRAW_THUMB_ARRAY_W+cx;
                int raw_x   = x + ( cx - ( WRAW_THUMB_ARRAY_W / 2 ) );
                int raw_y   = y + ( cy - ( WRAW_THUMB_ARRAY_H / 2 ) );
                int raw_idx = raw_y*raw_wid+raw_x;

                if ( ( raw_idx >= 0 ) && ( raw_idx < raw_max ) &&
                     ( raw_x >= 0 ) && ( raw_y >= 0 ) &&
                     ( raw_x <= raw_wid) && ( raw_y <= raw_hei) )
                {
                    sprintf( tmp_buf, "%5d" , ref_array[ raw_idx ] );

                    // Get Min/Max levels.
                    if ( minLevel < ref_array[ raw_idx ] )
                    {
                        minLevel = ref_array[ raw_idx ];
                    }

                    if ( maxLevel > ref_array[ raw_idx ] )
                    {
                        maxLevel = ref_array[ raw_idx ];
                    }
                }
                else
                {
                    sprintf( tmp_buf, "-" );
                }

                strRawThumbs[ mat_idx ] = tmp_buf;
                boxRawThumbs[ mat_idx ]->label( strRawThumbs[ mat_idx ].c_str() );
                boxRawThumbs[ mat_idx ]->tooltip( strRawThumbs[ mat_idx ].c_str() );
            }
        }

        levelDiff = maxLevel - minLevel;

        // Set max color gradiation to 200.
        float flvld = 256.0 / max_lval;

        for( int cy=0; cy<WRAW_THUMB_ARRAY_H; cy++ )
        {
            for( int cx=0; cx<WRAW_THUMB_ARRAY_W; cx++ )
            {
                int mat_idx = cy*WRAW_THUMB_ARRAY_W+cx;
                int raw_x   = x + ( cx - ( WRAW_THUMB_ARRAY_W / 2 ) );
                int raw_y   = y + ( cy - ( WRAW_THUMB_ARRAY_H / 2 ) );
                int raw_idx = raw_y*raw_wid+raw_x;

                if ( ( raw_idx >= 0 ) && ( raw_idx < raw_max ) &&
                     ( raw_x >= 0 ) && ( raw_y >= 0 ) &&
                     ( raw_x <= raw_wid) && ( raw_y <= raw_hei) )
                {
                    unsigned short refl  = ref_array[ raw_idx ];

                    if ( userevered == true )
                    {
                        refl = max_lval - ref_array[ raw_idx ];
                    }

                    unsigned tmplvl = refl * flvld;
                    if ( tmplvl > 255 )
                    {
                        tmplvl = 255;
                    }

                    unsigned char collvl = (unsigned char)tmplvl;

                    Fl_Color putc = fl_rgb_color( collvl, collvl, collvl );
                    Fl_Color lblc = fl_rgb_color( 255, 255 - collvl, 255 - collvl );

                    boxRawThumbs[ mat_idx ]->color( putc );
                    boxRawThumbs[ mat_idx ]->labelcolor( lblc );

                    if ( mat_idx == idx_div )
                    {
                        curRawPixel = ref_array[ raw_idx ];
                    }
                }
                else
                {
                    boxRawThumbs[ mat_idx ]->color( FL_BLACK );
                    boxRawThumbs[ mat_idx ]->labelcolor( FL_WHITE );
                }
            }
        }

        sprintf( outText,
                 "Coordination X:Y = %4d:%4d / 16bits level value = %d(0x%X)",
                 x,
                 y,
                 curRawPixel,
                 curRawPixel );

        boxRawThumbInfo->label( outText );
        windowRawThumb->show();
    }
}

void DialogMain::OnResized( float r )
{
    return;
}

void DialogMain::OnMove( int x, int y )
{
    if ( ( windowRawThumb != NULL ) && ( window != NULL ) )
    {
        if ( windowRawThumb->visible_r() == 0 )
        {
            windowRawThumb->position( window->x_root() + x + 10 ,
                                      window->y_root() + y + 10 );
        }
    }
}

void DialogMain::OnDraged( int x, int y )
{
    // nothing to do in this time...
}

void DialogMain::OnRuler( int startx, int starty, int endx, int endy, bool finished )
{
    float mult     = imgDspBox->multiplyratio();
    int distance_w = abs( endx - startx ) / mult;
    int distance_h = abs( endy - starty ) / mult;
    int distance   = sqrt( pow(distance_w,2) + pow(distance_h,2) );
    int angle      = 180.0 / 3.14 * atan2( ( endy - starty ), ( endx - startx ) );

    // Angle adjusting to Clock 12" = 0 degree.
    angle += 90;

    if ( angle < 0 )
    {
        angle += 360;
    }

    static char outstr[80] = {0};
    sprintf( outstr, "distance = %d pixels , angle = %d\n", distance, angle );

    imgDspBox->setrulerinfostring( outstr );

    if ( finished == true )
    {
        imgDspBox->redraw();
    }
}

void DialogMain::OnDropFile( const char* path )
{
    // Drag Drop Event !
    // check is this is dcm or not.
    string spath = path;

    // Make it double check for all platforms work.
    string::size_type fNL = spath.find( 0x0A );
    if ( fNL != string::npos )
    {
        spath = spath.substr( 0, fNL );
    }

    string checkext = spath;

    int fpos = checkext.find_last_of ( '.' );
    if ( fpos > 0 )
    {
        string fext = checkext.substr( fpos );
        transform( fext.begin(), fext.end(), fext.begin(), ::toupper );

        if ( fext == ".DCM" )
        {
            loadDCM( spath.c_str() );

            return;
        }
    }

    loadImage( spath.c_str() );
}

void DialogMain::OnClipBoardCopied()
{
#ifdef _WIN32
    X_MessageBoxTimeout( fl_xid( window ),
                         TEXT("Current view port copied to clipboard.\n(This message will disappear in 1 second)"),
                         TEXT("Clipboard copied"),
                         MB_ICONINFORMATION,
                         1000 );
#else
    fl_message_title( "Clipboard copied" );
    fl_message_hotspot( 1 );
    fl_message( "Current view port copied to clipboard." );
#endif // _WIN32
}

void DialogMain::OnKeyPressed( unsigned char k, int s, int c, int a )
{
    switch( k )
    {
        case '1':
            {
                float vmn = vsImageZoom->minimum();
                float vsf = vsImageZoom->value();

                if ( vsf != vmn )
                {
                    vsImageZoom->value( vmn );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;

        case '2':
            {
                float vsf = vsImageZoom->value();

                if ( vsf != 0.50f )
                {
                    vsImageZoom->value( 0.50f );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;

        case '3':
            {
                float vsf = vsImageZoom->value();

                if ( vsf != 0.75f )
                {
                    vsImageZoom->value( 0.75f );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;

        case '4':
            {
                float vsf = vsImageZoom->value();

                if ( vsf != 1.0f )
                {
                    vsImageZoom->value( 1.0f );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;

        case '5':
            {
                float vsf = vsImageZoom->value();

                if ( vsf != 1.25f )
                {
                    vsImageZoom->value( 1.25f );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;

        case '6':
            {
                float vsf = vsImageZoom->value();

                if ( vsf != 1.50f )
                {
                    vsImageZoom->value( 1.50f );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;

        case '7':
            {
                float vsf = vsImageZoom->value();

                if ( vsf != 1.75f )
                {
                    vsImageZoom->value( 1.75f );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;

        case '8':
            {
                float vsf = vsImageZoom->value();

                if ( vsf != 2.0f )
                {
                    vsImageZoom->value( 2.0f );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;

        case '9':
            {
                float vsf = vsImageZoom->value();

                if ( vsf != 3.0f )
                {
                    vsImageZoom->value( 3.0f );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;

        case '0':
            {
                float vmx = vsImageZoom->maximum();
                float vsf = vsImageZoom->value();

                if ( vsf != vmx )
                {
                    vsImageZoom->value( vmx );
                    ProcWidgetCB( vsImageZoom );
                }
            }
            break;

    }
}

void DialogMain::OnMouseWheel( int vect_v, int vect_h )
{
    // Test working on or not.
    if ( ( grpMain->active_r() == 0 ) || ( grpOverlay->visible_r() > 0 ) )
        return;

    // Checks zoom thread in work or not.
    if ( workingnow == true )
        return;

    const  unsigned minTick = 500;
    static unsigned prevTick = 0;
#ifdef _WIN32
    unsigned        curTick = GetTickCount();
#else
    unsigned        curTick = 0;
#endif /// of _WIN32
    if ( prevTick == 0 )
    {
        // May first time.
        prevTick = curTick - minTick;
    }

    if ( ( curTick - prevTick ) < minTick )
        return;

    double min_r     = vsImageZoom->minimum() + vsImageZoom->step();
    double max_r     = vsImageZoom->maximum() - vsImageZoom->step();
    double current_r = vsImageZoom->value();
    double step_r    = vsImageZoom->step() * 2;

    if ( vect_h > 0 )
    {
        // decrease ...
        if ( current_r > min_r )
        {
            if ( ( current_r - step_r ) > min_r )
            {
                vsImageZoom->value( current_r - step_r );
                vsImageZoom->do_callback();
            }
        }
    }
    else
    if( vect_h < 0 )
    {
        // increase ...
        if ( current_r < max_r )
        {
            if ( ( current_r + step_r ) < max_r )
            {
                vsImageZoom->value( current_r + step_r );
                vsImageZoom->do_callback();
            }
        }
    }

    prevTick = curTick;
}

////////////////////////////////////////////////////////////////////////////////

void fl_widget_cb(Fl_Widget* w, void* p)
{
    if ( p!= NULL )
    {
        DialogMain* pDM = (DialogMain*)p;
        pDM->ProcWidgetCB( w );
    }
}

static void fl_timer_cb( void* p )
{
#ifdef DEBUG
    printf( "#DEBUG fl_timer_cb() in\n" );
#endif
    if ( p != NULL )
    {
        DialogMain* pWin = (DialogMain*)p;
        pWin->ProcRedrawTimer();
    }
#ifdef DEBUG
    printf( "#DEBUG fl_timer_cb() out\n" );
#endif
}

void preload_cb(void* p)
{
#ifdef DEBUG
    printf("#DEBUG preload_cb() in\n" );
#endif
    if ( p!= NULL )
    {
        DialogMain* pDM = (DialogMain*)p;
        pDM->PreloadCB();
    }
#ifdef DEBUG
    printf( "#DEBUG preload_cb() out\n" );
#endif
}

void* pt_zoom_cb(void* p)
{
#ifdef DEBUG
    printf("#DEBUG pt_zoom_cb() in\n" );

    void* retv = NULL;
#endif
    if ( p!= NULL )
    {
        DialogMain* pDM = (DialogMain*)p;
#ifdef DEBUG
        retv = pDM->ProcZoomCB();
#else
        return pDM->ProcZoomCB();
#endif 
    }
#ifdef DEBUG
    printf("#DEBUG pt_zoom_cb() out\n" );
    
    return retv;
#endif
    return NULL;
}

void* pt_exportimg_cb( void* p )
{
#ifdef DEBUG
	printf("#DEBUG#, pt_exportimg_cb( %X );\n", p );
#endif
	if ( p!= NULL )
    {
        DialogMain* pDM = (DialogMain*)p;
        return pDM->ProcExportImgCB();
    }

    return NULL;
}
