#ifndef __DLGMAIN_H__
#define __DLGMAIN_H__

#include <pthread.h>

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Int_Input.H>
#include <FL/Fl_Slider.H>
#include <FL/Fl_Spinner.H>
#include <FL/Fl_Light_Button.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Output.H>
#include <FL/Fl_Choice.H>
#include <FL/Fl_Menu_Button.H>

// using customized FLTK components.
#include "fl/Fl_ImageViewer.h"
#include "fl/Fl_HistogramDrawer.h"
#include "fl/Fl_TransBox.h"

#include "rawprocessor.h"

class DialogMain : public Fl_ImageViewerNotifier
{
    public:
        DialogMain(int argc, char** argv);
        virtual ~DialogMain();

    public:
        int   Run();
        void  ProcWidgetCB( Fl_Widget* w );
        void* ProcZoomCB();
        void  PreloadCB();
        void* ProcExportImgCB();
#ifdef SUPPORT_DRAGDROP
        void  ProcDragDrop();
#endif /// of SUPPORT_DRAGDROP
        void  ProcRedrawTimer();

    public: /// embody for inherits Fl_ImageDisplayBoxUserOverride
        void drawuserimage(int w, int h);

    private:
        void createComponents();
        void createSubCompoents();

    private:
        void loadConfig();
        void saveConfig();
        bool load8bit( const char* ref, char* &newbuff, unsigned &newsz );
        void loadImage( const char* ref = NULL );
        void loadDCM( const char* ref = NULL );
        void updateImageInfo();
        void importRAWimage( bool useThreshold = false);
        void thresholdControl();
        void thresholdControlModify( int ccnt = 0 );
        void heightApply();
        void drawHistogram();
        void displayzoomratio();
        void updateFitButtonState( int seq = 0 );
        void clearLog();
        void addLog( const char* t );
        void reserveRedraw();

    private:
        int     _argc;
        char**  _argv;

    private:
        bool proceedingredraw;

    protected: /// embody for inherits Fl_ImageViewerNotifier
        void OnRightClick( int x, int y);
        void OnResized( float r );
        void OnDraged( int x, int y );
        void OnMove( int x, int y );
        void OnRuler( int startx, int starty, int endx, int endy, bool finished );
        void OnDropFile( const char* path );
        void OnClipBoardCopied();
        void OnKeyPressed( unsigned char k, int s, int c, int a );
        void OnMouseWheel( int vect_v, int vect_h );

    protected:
        Fl_Double_Window*       window;
        Fl_Group*               grpMain;
        Fl_Group*               grpOverlay;
        Fl_ImageViewer*         imgDspBox;
        Fl_Spinner*             vsImageZoom;
        Fl_Int_Input*           inpImageZoomWidth;
        Fl_Int_Input*           inpImageZoomHeight;
        Fl_Button*              btnFitToWidth;
        Fl_Button*              btnFitToOrigin;
        Fl_Choice*              chsResizeType;
        Fl_Light_Button*        cbUseThreshold;
        Fl_Light_Button*        cbReverseRender;
        Fl_HistogramDrawer*     boxHistogram;
        Fl_Input*               inpWSize;
        Fl_Input*               inpHSize;
        Fl_Button*              btnLoadImage;
        Fl_Menu_Button*         btnSaveImage;
        Fl_TransBox*            boxDspWait;

        Fl_Double_Window*       windowLog;
        Fl_Text_Display*        logImageInfo;
        Fl_Text_Buffer*         bufImageInfo;

        #define WRAW_THUMB_ARRAY_W      19
        #define WRAW_THUMB_ARRAY_H      13
        #define WRAW_THUMB_ARRAY_SZ     WRAW_THUMB_ARRAY_W*WRAW_THUMB_ARRAY_H

        Fl_Double_Window*       windowRawThumb;
        Fl_Box*                 boxRawThumbs[ WRAW_THUMB_ARRAY_SZ + 1 ];
        std::string             strRawThumbs[ WRAW_THUMB_ARRAY_SZ + 1 ];
        Fl_Box*                 boxRawThumbInfo;
        Fl_Button*              btnCloseRawThumb;

        //Fl_Double_Window*       windowPartHisto;

    protected:
        RAWProcessor*           mediRAW;
        RAWProcessor::\
        WeightAnalysisReport    weight_report;
        bool                    nowloading;
        bool                    loadtypeDCM;
        bool                    needpreload;
        bool                    userevered;
        bool                    userzoom;
        bool                    workingnow;
        int                     preloadheight;
        int                     preloadx;
        int                     preloady;
        int                     preloadw;
        int                     preloadh;
        int                     rotatetype;
        unsigned int            rotatemask;
        bool                    useinverted;
        bool                    useswap;
        bool                    use8bit;
        int                     v_thld_min;
        int                     v_thld_max;
        int                     exportparameter;
        pthread_t               pt_id_zoom;
        pthread_t               pt_id_threshold;
        pthread_t               pt_id_exportimage;

    protected:
        bool                    ucol_enabled;
        unsigned int            ucol_iv_minimap;
        unsigned int            ucol_hg_back;
        unsigned int            ucol_hg_sel;
        unsigned int            ucol_hg_histo;
        unsigned int            ucol_hg_histosel;

    protected:
        std::string             preloadfilename;
        std::string             rawimagefilename;
        unsigned char*          downscaled_raw_buffer;
};

#endif // of __DLGMAIN_H__
