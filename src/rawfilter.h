#ifndef __RAWFILTER_H__
#define __RAWFILTER_H__

#include <vector>

class rawimagefilter_if
{
    public:
        virtual
        void dofilter16( int width,
                         int height,
                         std::vector<unsigned short> &src,
                         std::vector<unsigned short> &dst ) = 0;
};

class basicfilter16 : public rawimagefilter_if
{
    public:
        void dofilter16( int width,
                         int height,
                         std::vector<unsigned short> &src,
                         std::vector<unsigned short> &dst );
};

#endif // __RAWFILTER_H__
