#ifndef __FL_HISTOGRAMDRAWER_H__
#define __FL_HISTOGRAMDRAWER_H__

#include <FL/Fl.H>
#include <FL/Fl_Box.H>

#include <vector>

class Fl_HistogramDrawer : public Fl_Box
{
    public:
        Fl_HistogramDrawer(int x,int y,int w,int h);
        virtual ~Fl_HistogramDrawer();

    public:
        void data( std::vector<unsigned int> &refdata );
        void emphase_value(int vmin, int vmax );

    public:
        void emphase( bool toggle )     { flag_emphase = toggle; draw(); }
        bool emphase()                  { return flag_emphase; }
        void limit( int l )             { val_limit = l; }
        int  limit()                    { return val_limit; }
        void ignoremaxlevel( int l )    { val_ignore_max_level = l; }
        int  ignoremaxlevel()           { return val_ignore_max_level ; }
        int  lastclick_x()              { return check_x; }
        int  lastclick_y()              { return check_y; }
        int  lastclick_btn()            { return mouse_btn; }

    public:
        void color_histogram( Fl_Color c ) { col_histo = c; }
        void color_selection( Fl_Color c ) { col_selection = c; }
        void color_selecthistogram( Fl_Color c ) { col_selhisto = c; }

    public:
        Fl_Color color_histogram()      { return col_histo; }
        Fl_Color color_selection()      { return col_selection; }
        Fl_Color color_selecthistogram(){ return col_selhisto; }

    protected: /// FLTK inherited
        int  handle( int event );
        void draw();
        void resize(int,int,int,int);

    protected:
        void findmax( unsigned minlevel = 0 );

    protected:
        Fl_Color            col_histo;
        Fl_Color            col_selection;
        Fl_Color            col_selhisto;

    private:
        int                 val_limit;
        int                 val_max;
        int                 val_emphase_min;
        int                 val_emphase_max;
        int                 val_ignore_max_level;
        bool                flag_emphase;
        bool                flag_drawing;
        bool                view_cpick;
        int                 check_x;
        int                 check_y;
        int                 cpick_x;
        int                 cpick_y;
        int                 mouse_btn;
        std::vector<int>    histo;

};

#endif /// of __FL_HISTOGRAMDRAWER_H__
