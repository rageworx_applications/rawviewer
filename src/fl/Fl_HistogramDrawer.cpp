#include <FL/fl_draw.H>
#include <FL/fl_ask.H>
#include "Fl_HistogramDrawer.h"
#include "fl_imgtk.h"

#ifndef MIN
    #define MIN(a,b) (((a)<(b))?(a):(b))
#endif

#ifndef MAX
    #define MAX(a,b) (((a)>(b))?(a):(b))
#endif

#define HISTO_AVERAGE_DEPTH

using namespace std;

Fl_HistogramDrawer::Fl_HistogramDrawer(int x,int y,int w,int h)
 : Fl_Box(x, y, w, h),
   val_emphase_min( 0 ),
   val_emphase_max( 0 ),
   val_limit( 0 ),
   val_ignore_max_level( 60000 ),
   flag_emphase( false ),
   flag_drawing( false ),
   view_cpick( false ),
   mouse_btn( 0 ),
   check_x( 0 ),
   check_y( 0 ),
   col_histo( 0x77000000 ),
   col_selection( 0x33333300 ),
   col_selhisto( 0xFF000000 )
{
    //box( FL_THIN_DOWN_FRAME );
    box( FL_NO_BOX );
    histo.clear();
}

Fl_HistogramDrawer::~Fl_HistogramDrawer()
{
    histo.clear();
}

void Fl_HistogramDrawer::data( vector<unsigned int> &refdata )
{
    if ( flag_drawing == true )
        return;

    histo.clear();
    histo.reserve( refdata.size() );

    for ( unsigned cnt=0; cnt<refdata.size(); cnt++ )
    {
        histo.push_back( refdata[cnt] );
    }

    findmax();
}

void Fl_HistogramDrawer::emphase_value(int vmin, int vmax )
{
    val_emphase_min = vmin;
    val_emphase_max = vmax;
}

void Fl_HistogramDrawer::findmax( unsigned minlevel )
{
    val_max = 0;

    unsigned hmin = minlevel;
    unsigned hmax = histo.size();

    if ( hmin > hmax )
        hmin = 0;

    for( unsigned cnt=minlevel; cnt<hmax; cnt++ )
    {
        int curVal = histo[cnt];
        if ( curVal < val_ignore_max_level )
        {
            if ( curVal > val_max )
                val_max = curVal;
        }
    }

    if ( val_max > val_ignore_max_level )
    {
        val_max = val_ignore_max_level;
    }
}

void Fl_HistogramDrawer::resize(int XX,int YY,int WW,int HH)
{
    Fl_Box::resize(XX,YY,WW,HH);
    redraw();
}

int Fl_HistogramDrawer::handle( int event )
{
    int ret = Fl_Box::handle( event );

    switch( event )
    {
        case FL_MOVE:
            if ( view_cpick == true )
            {
                cpick_x = Fl::event_x() - x();
                cpick_y = Fl::event_y() - y();

                redraw();
            }

        case FL_ENTER:
            if( histo.size() > 0 )
            {
                if ( view_cpick == false )
                {
                    view_cpick = true;
                }

                fl_cursor( FL_CURSOR_CROSS );
            }
            return 1;

        case FL_LEAVE:
            if( histo.size() > 0 )
            {
                if ( view_cpick == true )
                {
                    view_cpick = false;
                }

                fl_cursor( FL_CURSOR_DEFAULT );

                redraw();
            }
            return 1;

        case FL_RELEASE:
            mouse_btn = Fl::event_button();
            check_x = Fl::event_x() - x();
            check_y = Fl::event_y() - y();
            do_callback();
            break;
    }

    return ret;
}

void Fl_HistogramDrawer::draw()
{
    if ( flag_drawing == true )
        return;

    flag_drawing = true;

    fl_push_clip( x(), y(), w(), h() );

    // Erase bg.
    fl_color( FL_BLACK );
    fl_rectf( x(), y(), w(), h() );

    int histo_size = histo.size();

    if ( val_limit > 0 )
    {
        if ( val_limit < histo_size )
        {
            histo_size = val_limit;
        }
    }

    if ( histo_size > 0 )
    {
        fl_line_style( FL_SOLID );

        // Draw Histogram.
        float refW    = float( w() );
        float refH    = float( h() );
        float wscalef = float( histo_size ) / refW;
        float dsc_max = float( val_max );
        float dscalef = float( refH ) / dsc_max;

        bool lighterb = false;

#ifdef HISTO_AVERAGE_DEPTH
        int   minrpt  = histo_size / w();

        if ( minrpt < 1 )
            minrpt = 1;
#endif // HISTO_AVERAGE_DEPTH

        for(int cntH=0; cntH<w(); cntH++)
        {
            int refIdx = float( cntH ) * wscalef;
            int depth  = histo[refIdx] * dscalef;

#ifdef HISTO_AVERAGE_DEPTH
            int minIdx = refIdx-(minrpt/2);
            double depthSum = 0;

            if ( minIdx < 0 )
            {
                minIdx = 0;
            }

            for( int ccnt=minIdx; ccnt<minIdx+minrpt; ccnt++ )
            {
                depthSum += histo[ccnt] * dscalef;
            }

            depth = ( depthSum / (double)minrpt ) + 0.5;
#endif // HISTO_AVERAGE_DEPTH

            if ( ( view_cpick == true ) && ( cntH == cpick_x ) )
            {
                lighterb = true;
            }
            else
            {
                lighterb = false;
            }

            if ( flag_emphase == true )
            {
                // if current index in selection ...
                if ( ( val_emphase_min <= refIdx ) &&
                     ( val_emphase_max >= refIdx ) )
                {
                    if ( lighterb == true )
                    {
                        fl_color( fl_lighter( col_selection ) );
                    }
                    else
                    {
                        fl_color( col_selection );
                    }

                    fl_line( x() + cntH,
                             y() ,
                             x() + cntH,
                             y() + h() );

                    if ( lighterb == true )
                    {
                        fl_color( fl_lighter( col_selhisto ) );
                    }
                    else
                    {
                        fl_color( col_selhisto );
                    }
                }
                else
                {
                    if ( lighterb == true )
                    {
                        fl_color( fl_lighter( color() ) );
                        fl_line( x() + cntH,
                                 y() ,
                                 x() + cntH,
                                 y() + h() );
                        fl_color( fl_lighter( col_histo ) );
                    }
                    else
                    {
                        fl_color( col_histo );
                    }
                }
            }
            else
            {
                fl_color( col_histo );
            }

            fl_line( x() + cntH,
                     y() + ( h() - depth ),
                     x() + cntH,
                     y() + h() );

        }

        if ( view_cpick == true )
        {
            static char cpick_str[32] = {0};

            int refIdx = float( cpick_x ) * wscalef;

            sprintf( cpick_str, "%d(%d)", refIdx, histo[ refIdx ] );

            int put_x = x() + cpick_x + 10;
            int put_y = y() + cpick_y + 10;
            int mea_x = 0;
            int mea_y = 0;

            fl_measure( cpick_str, mea_x, mea_y, 0 );

            if ( ( put_x + mea_x ) > ( x() + w() ) )
            {
                put_x = x() + cpick_x - mea_x;
            }

            if ( ( put_y + mea_y ) > ( y() + h() ) )
            {
                put_y = y() + cpick_y - mea_y;
            }

            fl_font( labelfont(), labelsize() );
            fl_color( labelcolor() );
            fl_draw( cpick_str, put_x, put_y );
        }
    }

    Fl_Box::draw();
    fl_pop_clip();
    flag_drawing = false;
}
