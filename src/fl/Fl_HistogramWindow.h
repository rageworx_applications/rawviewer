#ifndef __FL_HISTOGRAMWINDOW_H__
#define __FL_HISTOGRAMWINDOW_H__

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_RGB_Image.H>

#include <vector>
#if 0
class Fl_HistogramWindow
 : public Fl_Double_Window
{
    public:
        Fl_HistogramWindow( int W, int H, const char* T );

    private:
        ~Fl_HistogramWindow();

    public:
        void data( std::vector< unsigned short >* weights );

    protected:
        void generategraphy();

    protected:
        Fl_Box*     boxHistography;

}
#endif
#endif /// of __FL_HISTOGRAMWINDOW_H__w
