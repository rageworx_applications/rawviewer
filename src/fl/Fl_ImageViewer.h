#ifndef __FL_IMAGEVIEWER_H__
#define __FL_IMAGEVIEWER_H__

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>

class Fl_ImageViewerNotifier
{
    public:
        virtual void OnRightClick( int x, int y) = 0;
        virtual void OnResized( float r ) = 0;
        virtual void OnDraged( int x, int y ) = 0;
        virtual void OnMove( int x, int y ) = 0;
        virtual void OnRuler( int startx, int starty, int endx, int endy, bool finished ) = 0;
        virtual void OnDropFile( const char* path ) = 0;
        virtual void OnClipBoardCopied() = 0;
        virtual void OnKeyPressed( unsigned char k, int s, int c, int a ) = 0;
        virtual void OnMouseWheel( int vect_v, int vect_h ) = 0;
};

class Fl_ImageViewer : public Fl_Box
{
    public:
        Fl_ImageViewer(int x,int y,int w,int h);
        virtual ~Fl_ImageViewer();

    public:
        void image(Fl_Image* aimg);
        void unloadimage();
        void multiplyratio( float rf );
        void fitwidth();
        void fitheight();
        void box(Fl_Boxtype new_box);
        void type(uchar t);
        // resizemethod new types :
        // t => 0:Nearest, 1:BiLinear, 2:BiCubic, 3:Lanzcos3
        void resizemethod( int t, bool autoapply = true );
#ifdef _WIN32
        bool savetomonopng( const wchar_t* fpath );
        bool savetocolorpng( const wchar_t* fpath );
#else
        bool savetomonopng( const char* fpath );
        bool savetocolorpng( const char* fpath );
#endif
    public:
        Fl_Image* image()                               { return imgsrc; }
        float     multiplyratio()                       { return multiplier; }
        void      coordlinecolor( Fl_Color c )          { colorclicked = c; }
        Fl_Color  coordlinecolor()                      { return colorclicked; }
        void      minimapcolor( Fl_Color c )            { colorminimap = c; }
        Fl_Color  minimapcolor()                        { return colorminimap; }
        int       resizemethod()                        { return resize_type; }

    public:
        void      notifier( Fl_ImageViewerNotifier* p ) { _notifier = p; }
        Fl_ImageViewerNotifier* notifier()              { return _notifier; }

    public:
        int imgw();
        int imgh();
        int prochandle( int event );

    public:
        void setrulerinfostring( const char* s );

    protected:
        typedef struct      { int x; int y; int w; int h; }dt_rect;
        dt_rect             rect_minimapout;
        dt_rect             rect_minimapin;
        bool                flag_showminimap;

    private:/// FLTK inherited ...
        int handle( int event );
        void draw();
        void resize(int,int,int,int);

    private:
        float       multiplier;

    private:
        Fl_Image*       imgsrc;
        Fl_Image*       imgcached;
        Fl_RGB_Image*   imgparthisto;

    private:
        bool        isdrawclickpointer;
        bool        isdrawruler;
        bool        isgeneratingimg;
        int         coordclicked_x;
        int         coordclicked_y;
        int         coordmoving_x;
        int         coordmoving_y;
        Fl_Color    colorclicked;
        Fl_Color    colortrace;
        Fl_Color    colorminimap;
        int         margin_x;
        int         margin_y;
        int         current_sx;
        int         current_sy;
        int         resize_type;
        bool        pushed_drag;
        char*       ruler_string;

    private:
        Fl_ImageViewerNotifier* _notifier;
};


#endif /// of __FL_IMAGEVIEWER_H__
