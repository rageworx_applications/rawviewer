#include <cmath>
#include <cwchar>
#include <FL/Fl_Copy_Surface.H>
#include <FL/fl_draw.H>
#ifdef USE_SYSPNG
#include <png.h>
#else
#include <FL/images/png.h>
#endif /// of USE_SYSPNG
#include "Fl_ImageViewer.h"
#include "fl_imgtk.h"

////////////////////////////////////////////////////////////////////////////////

#ifndef MIN
    #define MIN(a,b) (((a)<(b))?(a):(b))
#endif

#ifndef MAX
    #define MAX(a,b) (((a)>(b))?(a):(b))
#endif

#define MINIMAP_MAX_DISTANCE    150
#define MOVING_AMOUNT_DIVIDER   10

////////////////////////////////////////////////////////////////////////////////


Fl_ImageViewer::Fl_ImageViewer(int x,int y,int w,int h)
 : Fl_Box( x, y, w, h ),
   multiplier( 1.0f ),
   imgsrc( NULL ),
   imgcached( NULL ),
   imgparthisto( NULL ),
   _notifier( NULL ),
   isdrawclickpointer( false ),
   isdrawruler( false ),
   isgeneratingimg( false ),
   colorclicked( FL_RED ),
   colortrace( FL_GRAY ),
   colorminimap( 0xFF555500 ),
   margin_x( 0 ),
   margin_y( 0 ),
   current_sx( 0 ),
   current_sy( 0 ),
   resize_type( 0 ),
   pushed_drag( false ),
   flag_showminimap( false ),
   ruler_string( NULL )
{
    //box( FL_THIN_DOWN_FRAME );
    Fl_Box::box( FL_NO_BOX );
    color( FL_BLACK );
    type( 0 );

    labelcolor( FL_WHITE );
    label( "Drag drop RAW & DCM file to here" );
}

Fl_ImageViewer::~Fl_ImageViewer()
{
    if ( ruler_string != NULL )
    {
        free( ruler_string );
    }

    unloadimage();
}

void Fl_ImageViewer::image(Fl_Image* aimg)
{
    if( aimg == NULL )
        return;

    unloadimage();

    imgsrc = aimg->copy( aimg->w(), aimg->h() );

    if ( imgsrc != NULL )
    {
        label( NULL );

        // make minimap out.
        if ( imgsrc->w() < imgsrc->h() )
        {
            rect_minimapout.h = MINIMAP_MAX_DISTANCE;
            rect_minimapout.w = MINIMAP_MAX_DISTANCE *
                                ( (float)imgsrc->w() / (float)imgsrc->h() );
        }
        else
        {
            rect_minimapout.w = MINIMAP_MAX_DISTANCE;
            rect_minimapout.h = MINIMAP_MAX_DISTANCE *
                                ( (float)imgsrc->h() / (float)imgsrc->w() );
        }

        redraw();
    }
}

void Fl_ImageViewer::unloadimage()
{
    if( imgcached != NULL )
    {
        Fl_RGB_Image* refimg = (Fl_RGB_Image*)imgcached;

        if ( ( refimg->array != NULL ) && ( refimg->alloc_array == 0 ) )
        {
            delete[] refimg->array;
        }

        delete imgcached;
        imgcached = NULL;
    }

    if ( imgsrc != NULL )
    {
        Fl_RGB_Image* refimg = (Fl_RGB_Image*)imgsrc;

        if ( ( refimg->array != NULL ) && ( refimg->alloc_array == 0 ) )
        {
            delete[] refimg->array;
        }

        delete imgsrc;
        imgsrc = NULL;
    }
}

void Fl_ImageViewer::multiplyratio( float rf )
{
    if ( imgsrc == NULL )
        return;

    if ( isgeneratingimg == true )
        return;

    isgeneratingimg = true;

    bool  recalc_center = false;
    float rc_x = 0.0f;
    float rc_y = 0.0f;

    if ( imgcached != NULL )
    {
        if ( ( current_sx > 0 ) || ( current_sy > 0 ) )
        {
            rc_x = (float)( current_sx + ( w() / 2 ) ) / (float)imgcached->w();
            rc_y = (float)( current_sy + ( h() / 2 ) ) / (float)imgcached->h();
#ifdef DEBUG
            printf( "rc_x = %f , rc_y = %f\n", rc_x, rc_y );
#endif // DEBUG
            recalc_center = true;
        }

        Fl_RGB_Image* refimg = (Fl_RGB_Image*)imgcached;

        if ( ( refimg->array != NULL ) && ( refimg->alloc_array == 0 ) )
        {
            delete[] refimg->array;
        }

        delete imgcached;

        imgcached = NULL;
    }

    multiplier   = rf;
    float f_w    = imgsrc->w() * multiplier;
    float f_h    = imgsrc->h() * multiplier;

    imgcached = fl_imgtk::rescale( (Fl_RGB_Image*)imgsrc,
                                   f_w, f_h,
                                   (fl_imgtk::rescaletype)resize_type );

    if ( isdrawclickpointer == true )
    {
        isdrawclickpointer =false;
    }

    if ( isdrawruler == true )
    {
        isdrawruler = false;

        if ( ruler_string != NULL )
        {
            free( ruler_string );
            ruler_string = NULL;
        }
    }

    // Check drawing positions ..
    if ( imgcached != NULL )
    {
        if ( ( current_sx != 0 ) || ( current_sy != 0 ) )
        {

            if ( recalc_center == true )
            {
                current_sx = ( imgcached->w() * rc_x ) - ( w() / 2 );
                current_sy = ( imgcached->h() * rc_y ) - ( h() / 2 );

                if ( current_sx < 0 )
                {
                    current_sx = 0;
                }

                if ( current_sy < 0 )
                {
                    current_sy = 0;
                }
            }

            if ( imgcached->w() < w() )
            {
                current_sx = 0;
            }
            else
            if ( current_sx + w() > imgcached->w() )
            {
                current_sx = imgcached->w() - w();
            }

            if ( imgcached->h() < h() )
            {
                current_sy = 0;
            }
            else
            if ( current_sy + h() > imgcached->h() )
            {
                current_sy = imgcached->h() - h();
            }
        }

        if ( pushed_drag == true )
        {
            pushed_drag = false;
        }
    }

    isgeneratingimg = false;

    redraw();

    if ( _notifier != NULL )
    {
        _notifier->OnResized( multiplier );
    }
}

void Fl_ImageViewer::fitwidth()
{
    if ( imgsrc == NULL )
        return;

    float f_w = float( w() ) / float( imgsrc->w() );

    multiplyratio( f_w );
}

void Fl_ImageViewer::fitheight()
{
    if ( imgsrc == NULL )
        return;

    float f_h = float( h() ) / float( imgsrc->h() );

    multiplyratio( f_h );

}

int Fl_ImageViewer::imgw()
{
    if ( imgcached != NULL )
    {
        return imgcached->w();
    }

    return 0;
}

int Fl_ImageViewer::imgh()
{
    if ( imgcached != NULL )
    {
        return imgcached->h();
    }

    return 0;
}

int Fl_ImageViewer::prochandle( int event )
{
    return handle( event );
}

void Fl_ImageViewer::setrulerinfostring( const char* s )
{
    if ( ruler_string != NULL )
    {
        free( ruler_string );
    }

    ruler_string = strdup( s );
}

int Fl_ImageViewer::handle( int event )
{
    int ret = Fl_Box::handle( event );

    static int  mouse_btn;
    static int  shift_key;
    static int  check_x;
    static int  check_y;
    static int  pushed_drag_point_x;
    static int  pushed_drag_point_y;

    switch( event )
    {
        case FL_DND_ENTER:
        case FL_DND_DRAG:
        case FL_DND_RELEASE:
        case FL_DND_LEAVE:
            if ( _notifier != NULL )
            {
                ret = 1;
            }
            else
            {
                ret = 0;
            }
            break;

        case FL_PASTE:
            if ( _notifier != NULL )
            {
                //static const char* retC = Fl::event_text();
                char* retC = strdup( Fl::event_text() );
                if ( retC != NULL )
                {
                    _notifier->OnDropFile( retC );
                    free ( retC );
                }
                fl_cursor( FL_CURSOR_CROSS );

                ret = 1;
            }
            else
            {
                ret = 0;
            }
            break;

        case FL_PUSH:
            take_focus();
            shift_key = Fl::event_shift();
            mouse_btn = Fl::event_button();
            if ( mouse_btn == FL_LEFT_MOUSE )
            {
                if ( isdrawclickpointer == true )
                {
                    isdrawclickpointer = false;
                    redraw();
                }

                check_x = Fl::event_x() + margin_x;
                check_y = Fl::event_y() + margin_y;

                if ( ( shift_key > 0 ) && ( imgcached != NULL ) )
                {
                    coordclicked_x = check_x;
                    coordclicked_y = check_y;
                    isdrawruler = true;
                }
                else
                {
                    if ( isdrawruler == true )
                    {
                        isdrawruler = false;

                        if ( ruler_string != NULL )
                        {
                            free( ruler_string );
                            ruler_string = NULL;
                        }
                    }
                    pushed_drag_point_x = check_x + current_sx;
                    pushed_drag_point_y = check_y + current_sy;
                    pushed_drag = true;
                }
            }
            return 1;

        case FL_MOVE:
            check_x = Fl::event_x();
            check_y = Fl::event_y();

            if ( _notifier != NULL )
            {
                _notifier->OnMove( check_x, check_y );
            }
            return 1;


        case FL_DRAG:
            isdrawclickpointer = false;

            if ( isdrawruler == true )
            {
                coordmoving_x = Fl::event_x();
                coordmoving_y = Fl::event_y();

                int last_x = coordmoving_x + margin_x;
                int last_y = coordmoving_y + margin_y;

                if ( _notifier != NULL )
                {
                    _notifier->OnRuler( coordclicked_x,
                                        coordclicked_y,
                                        last_x,
                                        last_y,
                                        false );
                }


                this->redraw();
            }
            else
            if ( pushed_drag == true )
            {
                int mov_x = pushed_drag_point_x - Fl::event_x();
                int mov_y = pushed_drag_point_y - Fl::event_y();

                if ( imgcached != NULL )
                {
                    fl_cursor( FL_CURSOR_MOVE );

                    int me_w = this->w();
                    int me_h = this->h();

                    if ( ( imgcached->w() <= me_w ) && ( imgcached->h() <= me_h ) )
                    {
                        current_sx = 0;
                        current_sy = 0;
                    }
                    else
                    {
                        if ( imgcached->w() > me_w )
                        {
                            current_sx = mov_x;

                            if ( current_sx < 0 )
                            {
                                current_sx = 0;
                            }
                            else
                            {
                                if ( current_sx + me_w > imgcached->w() )
                                {
                                    current_sx = imgcached->w() - me_w;
                                }
                            }
                        }

                        if ( imgcached->h() > me_h )
                        {
                            current_sy = mov_y;

                            if ( current_sy < 0 )
                            {
                                current_sy = 0;
                            }
                            else
                            {
                                if ( current_sy + me_h > imgcached->h() )
                                {
                                    current_sy = imgcached->h() - me_h;
                                }
                            }
                        }
                    }

                    redraw();

                    if ( _notifier != NULL )
                    {
                        _notifier->OnDraged( mov_x, mov_y );
                    }
                }


            }
            return 1;

        case FL_ENTER:
            fl_cursor( FL_CURSOR_CROSS );
            return 1;

        case FL_LEAVE:
            fl_cursor( FL_CURSOR_DEFAULT );
            return 1;

        case FL_RELEASE:
            shift_key = Fl::event_shift();
            mouse_btn = Fl::event_button();
            fl_cursor( FL_CURSOR_CROSS );

            if ( mouse_btn == FL_LEFT_MOUSE )
            {
                if ( isdrawruler == true )
                {
                    check_x = Fl::event_x();
                    check_y = Fl::event_y();

                    int last_x = check_x + margin_x;
                    int last_y = check_y + margin_y;

                    if ( _notifier != NULL )
                    {
                        _notifier->OnRuler( coordclicked_x,
                                            coordclicked_y,
                                            last_x,
                                            last_y,
                                            true );
                    }
                }
                else
                if ( pushed_drag == true )
                {
                    pushed_drag = false;
                }
            }
            else
            if ( mouse_btn == FL_RIGHT_MOUSE )
            {
                if ( isdrawruler == true )
                {
                    isdrawruler = false;
                }

                if ( imgsrc != NULL )
                {
                    //check_x = Fl::event_x() - x() + margin_x;
                    check_x = Fl::event_x() - x() - margin_x;
                    //check_y = Fl::event_y() - y() + margin_y;
                    check_y = Fl::event_y() - y() - margin_y;

                    if ( check_x < 0 )
                    {
                        check_x = 0;
                    }

                    if ( check_y < 0 )
                    {
                        check_y = 0;
                    }

                    double f_x = double( current_sx + check_x ) / multiplier;
                    double f_y = double( current_sy + check_y ) / multiplier;

                    check_x = roundl( f_x );
                    check_y = roundl( f_y );

                    if ( check_x < 0 )
                    {
                        check_x = 0;
                    }
                    else
                    if ( check_x >= imgsrc->w() )
                    {
                        check_x = imgsrc->w();
                    }

                    if ( check_y < 0 )
                    {
                        check_y = 0;
                    }
                    else
                    if ( check_y >= imgsrc->h() )
                    {
                        check_y = imgsrc->h();
                    }

                    // I don't know why it is going wrong coordination,
                    // But it need to be doing minus 2 again for matching drawing line.
                    coordclicked_x = Fl::event_x() - margin_x;
                    coordclicked_y = Fl::event_y() - margin_y;
                    isdrawclickpointer = true;
                    redraw();

                    if ( _notifier != NULL )
                    {
                        _notifier->OnRightClick( check_x, check_y );
                    }
                }
            }

            return 1;

        case FL_FOCUS:
        case FL_UNFOCUS:
            return 1;

        //case FL_KEYUP:
        case FL_KEYDOWN:
            {
                bool bredraw = false;

                int kbda = Fl::event_alt();
                int kbdc = Fl::event_ctrl();
                int kbds = Fl::event_shift();
                int kbdk = Fl::event_key();
#ifdef __APPLE__
                if ( kbdc == 0 )
				{
					kbdc = Fl::event_command();
				}
#endif

                switch ( kbdk )
                {
                    case FL_Home:
                        if ( imgcached != NULL )
                        {
                            if ( imgcached->h() > h() )
                            {
                                if ( current_sy > 0 )
                                {
                                    current_sy = 0;
                                    bredraw = true;
                                }
                                else
                                // at top of screen, home key again ...
                                if ( ( current_sy == 0 ) && ( current_sx > 0 ) )
                                {
                                    current_sx = 0;
                                    bredraw = true;
                                }
                                else
                                if ( ( current_sy + h() == imgcached->h() ) &&
                                     ( current_sx > 0 ) )
                                {
                                    current_sx = 0;
                                    bredraw = true;
                                }
                            }
                        }
                        break;

                    case FL_End:
                        if ( imgcached != NULL )
                        {
                            if ( imgcached->h() > h() )
                            {
                                if ( current_sy + h() < imgcached->h() )
                                {
                                    current_sy = imgcached->h() - h();
                                    bredraw = true;
                                }
                                else
                                // at bottom of screen, home key again ...
                                if ( ( ( current_sy + h() ) == imgcached->h() ) &&
                                     ( ( current_sx + w() ) < imgcached->w() ) )
                                {
                                    if ( imgcached->w() > w() )
                                    {
                                        current_sx = imgcached->w() -  w();
                                        bredraw = true;
                                    }
                                }
                            }
                        }
                        break;

                    case FL_Page_Up:
                        if ( imgcached != NULL )
                        {
                            int scl_amt = h();

                            if ( kbdc != 0 )
                            {
                                scl_amt = imgcached->h() - h();
                            }

                            current_sy -= scl_amt;

                            if ( current_sy < 0 )
                            {
                                current_sy = 0;
                            }

                            bredraw = true;
                        }
                        break;

                    case FL_Page_Down:
                        if ( imgcached != NULL )
                        {
                            int scl_amt = h();

                            if ( kbdc != 0 )
                            {
                                scl_amt = imgcached->h() - h() - current_sy;
                            }

                            current_sy += scl_amt;

                            if ( current_sy > imgcached->h() - h() )
                            {
                                current_sy = imgcached->h() - h();
                            }

                            bredraw = true;
                        }
                        break;

                    case FL_Left:
                        if ( imgcached != NULL )
                        {
                            if ( imgcached->w() > w() )
                            {
                                if ( current_sx > 0)
                                {
                                    int mv_amt = w() / MOVING_AMOUNT_DIVIDER;

                                    if ( mv_amt == 0 )
                                    {
                                        mv_amt = MOVING_AMOUNT_DIVIDER;
                                    }

                                    if ( kbdc != 0 )
                                    {
                                        mv_amt = current_sx;
                                    }

                                    current_sx -= mv_amt;

                                    if ( current_sx < 0 )
                                    {
                                        current_sx = 0;
                                    }

                                    bredraw = true;
                                }
                            }
                        }
                        break;

                    case FL_Right:
                        if ( imgcached != NULL )
                        {
                            if ( imgcached->w() > w() )
                            {
                                if ( current_sx + w() < imgcached->w() )
                                {
                                    int mv_amt = w() / MOVING_AMOUNT_DIVIDER;

                                    if ( mv_amt == 0 )
                                    {
                                        mv_amt = MOVING_AMOUNT_DIVIDER;
                                    }

                                    if ( kbdc != 0 )
                                    {
                                        mv_amt = imgcached->w() - w() - current_sx;
                                    }

                                    current_sx += mv_amt;

                                    if ( current_sx > imgcached->w() - w() )
                                    {
                                        current_sx = imgcached->w() - w();
                                    }

                                    bredraw = true;
                                }
                            }
                        }
                        break;

                    case FL_Up:
                        if ( imgcached != NULL )
                        {
                            if ( imgcached->h() > h() )
                            {
                                if ( current_sy > 0)
                                {
                                    int mv_amt = h() / MOVING_AMOUNT_DIVIDER;

                                    if ( mv_amt == 0 )
                                    {
                                        mv_amt = MOVING_AMOUNT_DIVIDER;
                                    }

                                    if ( kbdc != 0 )
                                    {
                                        mv_amt = current_sy;
                                    }

                                    current_sy -= mv_amt;

                                    if ( current_sy < 0 )
                                    {
                                        current_sy = 0;
                                    }

                                    bredraw = true;
                                }
                            }
                        }
                        break;

                    case FL_Down:
                        if ( imgcached != NULL )
                        {
                            if ( imgcached->h() > h() )
                            {
                                if ( current_sy + h() < imgcached->h() )
                                {
                                    int mv_amt = h() / MOVING_AMOUNT_DIVIDER;

                                    if ( mv_amt == 0 )
                                    {
                                        mv_amt = MOVING_AMOUNT_DIVIDER;
                                    }

                                    if ( kbdc != 0 )
                                    {
                                        mv_amt = imgcached->h() - h() - current_sy;
                                    }

                                    current_sy += mv_amt;

                                    if ( current_sy > imgcached->h() - h() )
                                    {
                                        current_sy = imgcached->h() - h();
                                    }

                                    bredraw = true;
                                }
                            }
                        }
                        break;

                    case 0x20: /// Space bar : Goto center !
                        if ( imgcached != NULL )
                        {
                            if ( ( imgcached->h() > h() ) && ( imgcached->w() > w() ) )
                            {
                                current_sx = ( imgcached->w() - w() ) / 2;
                                current_sy = ( imgcached->h() - h() ) / 2;

                                bredraw = true;
                            }
                        }
                        break;

                    case 'c': /// Make Ctrl + C -> Copy current view to clipboard.
                        if ( imgcached != NULL )
                        {
                            if ( kbdc != 0 )
                            {
                                if ( kbds != 0 )
                                {
                                    // copy whole image !

                                    Fl_Copy_Surface* cpsurf =
                                        new Fl_Copy_Surface( imgcached->w(), imgcached->h() );

                                    if ( cpsurf != NULL )
                                    {
#ifdef DEBUG
                                        printf( "Copy surface created as %d x %d\n",
                                                cpsurf->w(), cpsurf->h() );
#endif
                                        cpsurf->set_current();
                                        //cpsurf->draw( img
                                        Fl_Color prevc = fl_color();
                                        fl_color( FL_BLACK );
                                        fl_rectf( 0,0, cpsurf->w(), cpsurf->h() );
                                        fl_color( prevc );

                                        imgcached->draw( 0 , 0 );
                                        delete cpsurf;

                                        Fl_Display_Device::display_device()->set_current();

                                        if ( _notifier != NULL )
                                        {
                                            _notifier->OnClipBoardCopied();
                                        }
                                    }
                                }
                                else
                                {
                                    int surfsz_w = w();
                                    int surfsz_h = h();
                                    int opt_x    = current_sx;
                                    int opt_y    = current_sy;

                                    if ( imgcached->w() < w() )
                                    {
                                        surfsz_w = imgcached->w();
                                        opt_x    = 0;
                                    }

                                    if ( imgcached->h() < h() )
                                    {
                                        surfsz_h = imgcached->h();
                                        opt_y    = 0;
                                    }

                                    Fl_Copy_Surface* cpsurf =
                                        new Fl_Copy_Surface( surfsz_w, surfsz_h );

                                    if ( cpsurf != NULL )
                                    {
#ifdef DEBUG
                                        printf( "Copy surface created as %d x %d\n",
                                                cpsurf->w(), cpsurf->h() );
#endif
                                        cpsurf->set_current();
                                        Fl_Color prevc = fl_color();
                                        fl_color( FL_BLACK );
                                        fl_rectf( 0,0, cpsurf->w(), cpsurf->h() );
                                        fl_color( prevc );

                                        if ( ( opt_x > 0 ) && ( opt_y > 0 ) )
                                        {
                                            imgcached->draw( 0 , 0, surfsz_w, surfsz_h, opt_x, opt_y );
                                        }
                                        else
                                        {
                                            imgcached->draw( 0 , 0 );
                                        }

                                        delete cpsurf;

                                        Fl_Display_Device::display_device()->set_current();

                                        if ( _notifier != NULL )
                                        {
                                            _notifier->OnClipBoardCopied();
                                        }
                                    }

                                }

                            }
                        }
                        break;

                    default:
                        if ( _notifier != NULL )
                        {
                            unsigned char kb = kbdk & 0xFF;

                            _notifier->OnKeyPressed( kb, kbds, kbdc, kbda );
                        }
                        break;

                }

                if ( bredraw == true )
                {
                    redraw();
                }

                ret = 1;
            }
            break;

        case FL_MOUSEWHEEL:
            {
                if ( _notifier != NULL )
                {
                    int whl_v = Fl::event_dx();
                    int whl_h = Fl::event_dy();

                    _notifier->OnMouseWheel( whl_v, whl_h );
                }
            }
            break;

    }

    return ret;
}

void Fl_ImageViewer::draw()
{
    fl_push_clip( x(), y(), w(), h() );

    Fl_Color prevC = fl_color();

    // Clear Background.
    fl_color( color() );
    fl_rectf( x(), y(), w(), h() );

    if ( ( imgcached != NULL ) && ( isgeneratingimg == false ) )
    {
        imgcached->draw( x() , y(), w(), h(), current_sx, current_sy );

        float f_w = (float)w() / (float)imgcached->w();
        float f_h = (float)h() / (float)imgcached->h();

        if ( ( f_w < 1.0f ) || ( f_h < 1.0f ) )
        {
            flag_showminimap = true;

            rect_minimapout.x = w() - rect_minimapout.w;
            rect_minimapout.y = h() - rect_minimapout.h;

            rect_minimapin.w = f_w * (float)(rect_minimapout.w) - 2;
            rect_minimapin.h = f_h * (float)(rect_minimapout.h) - 2;

            if ( rect_minimapin.w > rect_minimapout.w - 2 )
            {
                rect_minimapin.w = rect_minimapout.w - 2;
            }

            if ( rect_minimapin.h > rect_minimapout.h - 2 )
            {
                rect_minimapin.h = rect_minimapout.h - 2;
            }

            rect_minimapin.x = ( (float)current_sx / (float)imgcached->w() )
                               * (float)( rect_minimapout.w + 2 );
            rect_minimapin.y = ( (float)current_sy / (float)imgcached->h() )
                               * (float)( rect_minimapout.h + 2 );
            rect_minimapin.x += rect_minimapout.x + 1;
            rect_minimapin.y += rect_minimapout.y + 1;

            int x_limit = rect_minimapout.x + rect_minimapout.w - rect_minimapin.w - 2;
            int y_limit = rect_minimapout.y + rect_minimapout.h - rect_minimapin.h - 2;

            if ( rect_minimapin.x > x_limit )
            {
                rect_minimapin.x = x_limit + 1;
            }

            if ( rect_minimapin.y > y_limit )
            {
                rect_minimapin.y = y_limit + 1;
            }
        }
        else
        {
            flag_showminimap = false;
        }
    }

    if ( isdrawruler == true )
    {
        Fl_Color ruler_col      = 0xFF666600;
        Fl_Color ruler_txt_brdr = 0x00000000;

        fl_color( ruler_txt_brdr );
        fl_line_style( FL_SOLID , 4, NULL );
        fl_line( coordclicked_x, coordclicked_y, coordmoving_x, coordmoving_y );

        fl_color( ruler_col );
        fl_line_style( FL_SOLID, 1, NULL );
        fl_line( coordclicked_x, coordclicked_y, coordmoving_x, coordmoving_y );

        if ( ruler_string != NULL )
        {
            if ( strlen( ruler_string ) > 0 )
            {
                // Draw ruler on its component surface.
                fl_font( FL_COURIER, 12 );
                fl_color( ruler_col );

                int px = MIN( coordmoving_x, coordclicked_x );
                int py = 0;

                if ( coordmoving_y > coordclicked_y )
                {
                    py = MAX( coordmoving_y, coordclicked_y ) + 20;
                }
                else
                {
                    py = MIN( coordmoving_y, coordclicked_y ) - 20;
                }

                int rslen = strlen( ruler_string );

                fl_color( ruler_txt_brdr );
                fl_draw( ruler_string, rslen, px-1, py-1 );
                fl_draw( ruler_string, rslen, px+0, py-1 );
                fl_draw( ruler_string, rslen, px-1, py+0 );
                fl_draw( ruler_string, rslen, px+1, py-1 );
                fl_draw( ruler_string, rslen, px-1, py+1 );
                fl_draw( ruler_string, rslen, px+0, py+1 );
                fl_draw( ruler_string, rslen, px-1, py+0 );
                fl_draw( ruler_string, rslen, px+1, py+1 );

                fl_color( ruler_col );
                fl_draw( ruler_string, px, py );
            }
        }
    }
    else
    if ( isdrawclickpointer == true )
    {
        fl_color( colorclicked );
        fl_line_style( 0, 1, NULL );

        fl_line( coordclicked_x , y() , coordclicked_x, coordclicked_y - 1 );
        fl_line( coordclicked_x, coordclicked_y + 1 , coordclicked_x, y() + h() );
        fl_line( x(), coordclicked_y, coordclicked_x - 1, coordclicked_y );
        fl_line( coordclicked_x + 1, coordclicked_y, x() + w(), coordclicked_y );
    }

    if ( flag_showminimap == true )
    {
        fl_color( colorminimap );
        fl_rect( rect_minimapout.x, rect_minimapout.y,
                 rect_minimapout.w, rect_minimapout.h );

        fl_color( fl_lighter( colorminimap ) );
        fl_rect( rect_minimapin.x, rect_minimapin.y,
                 rect_minimapin.w, rect_minimapin.h );
    }

    // Draw itself rectangle.
    Fl_Box::draw();

    fl_color( prevC );

    fl_pop_clip();
}

void Fl_ImageViewer::resize(int x,int y,int w,int h)
{
    Fl_Box::resize( x, y, w, h );

    if ( imgcached != NULL )
    {
        if ( current_sx < 0 )
        {
            current_sx = 0;
        }

        if ( current_sy < 0 )
        {
            current_sy = 0;
        }

        if ( imgcached->w() > w )
        {
            if ( current_sx > imgcached->w() - w )
            {
                current_sx = imgcached->w() - w;
            }
        }

        if ( imgcached->h() > h )
        {
            if ( current_sy > imgcached->h() - h )
            {
                current_sy = imgcached->h() - h;
            }
        }
    }
}

void Fl_ImageViewer::box(Fl_Boxtype new_box)
{
    // It always ignore user custom box type.
    //Fl_Scroll::box( box() );
    Fl_Box::box( FL_THIN_DOWN_FRAME );
}

void Fl_ImageViewer::type(uchar t)
{
    // It always ignore user type = NO scroll bars.
    // Fl_Scroll::type( type() );
    Fl_Box::type( 0 );
}

void Fl_ImageViewer::resizemethod( int t, bool autoapply )
{
    if ( t <= 3 )
    {
        if ( resize_type != t )
        {
            resize_type = t;

            if ( autoapply == true )
            {
                multiplyratio( multiplier );
            }
        }
    }
}

#ifdef _WIN32
bool Fl_ImageViewer::savetomonopng( const wchar_t* fpath )
{
    if ( imgcached == NULL )
        return false;

    FILE* fp = _wfopen( fpath, L"wb" );
#else
bool Fl_ImageViewer::savetomonopng( const char* fpath )
{
	if ( imgcached == NULL )
	    return false;

	FILE* fp = fopen( fpath, "wb" );
#endif /// of _WIN32
    if ( fp == NULL )
        return false;

    png_structp png_ptr     = NULL;
    png_infop   info_ptr    = NULL;
    png_bytep   row         = NULL;

    png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
    if ( png_ptr != NULL )
    {
        info_ptr = png_create_info_struct( png_ptr );
        if ( info_ptr != NULL )
        {
            if ( setjmp( png_jmpbuf( (png_ptr) ) ) == 0 )
            {
                int mx = imgcached->w();
                int my = imgcached->h();
                int pd = imgcached->d();

                png_init_io( png_ptr, fp );
                png_set_IHDR( png_ptr,
                              info_ptr,
                              mx,
                              my,
                              8,
                              PNG_COLOR_TYPE_GRAY,
                              PNG_INTERLACE_NONE,
                              PNG_COMPRESSION_TYPE_BASE,
                              PNG_FILTER_TYPE_BASE);

                png_write_info( png_ptr, info_ptr );

                row = (png_bytep)malloc( imgcached->w() * sizeof( png_byte ) );
                if ( row != NULL )
                {
                    const char* buf = imgcached->data()[0];
                    int bque = 0;

                    for( int y=0; y<my; y++ )
                    {
                        for( int x=0; x<mx; x++ )
                        {
                            //int bque = y * mx  + ( x * pd );
                            row[ x ] = buf[ bque ];
                            bque += pd;
                        }

                        png_write_row( png_ptr, row );
                    }

                    png_write_end( png_ptr, NULL );

                    fclose( fp );

                    free(row);
                }

                png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

                return true;
            }
        }
    }
}

#ifdef _WIN32
bool Fl_ImageViewer::savetocolorpng( const wchar_t* fpath )
{
    if ( imgcached == NULL )
        return false;

    if ( imgcached->d() < 3 )
        return false;

    FILE* fp = _wfopen( fpath, L"wb" );
#else
bool Fl_ImageViewer::savetocolorpng( const char* fpath )
{
	if ( imgcached == NULL )
	return false;

	if ( imgcached->d() < 3 )
	return false;

	FILE* fp = fopen( fpath, "wb" );
#endif /// of _WIN32
    if ( fp == NULL )
        return false;

    png_structp png_ptr     = NULL;
    png_infop   info_ptr    = NULL;
    png_bytep   row         = NULL;

    png_ptr = png_create_write_struct( PNG_LIBPNG_VER_STRING, NULL, NULL, NULL );
    if ( png_ptr != NULL )
    {
        info_ptr = png_create_info_struct( png_ptr );
        if ( info_ptr != NULL )
        {
            if ( setjmp( png_jmpbuf( (png_ptr) ) ) == 0 )
            {
                int mx = imgcached->w();
                int my = imgcached->h();
                int pd = 3;

                png_init_io( png_ptr, fp );
                png_set_IHDR( png_ptr,
                              info_ptr,
                              mx,
                              my,
                              8,
                              PNG_COLOR_TYPE_RGB,
                              PNG_INTERLACE_NONE,
                              PNG_COMPRESSION_TYPE_BASE,
                              PNG_FILTER_TYPE_BASE);

                png_write_info( png_ptr, info_ptr );

                row = (png_bytep)malloc( imgcached->w() * sizeof( png_byte ) * 3 );
                if ( row != NULL )
                {
                    const char* buf = imgcached->data()[0];
                    int bque = 0;

                    for( int y=0; y<my; y++ )
                    {
                        for( int x=0; x<mx; x++ )
                        {
                            row[ (x*3) + 0 ] = buf[ bque + 0 ];
                            row[ (x*3) + 1 ] = buf[ bque + 1 ];
                            row[ (x*3) + 2 ] = buf[ bque + 2 ];
                            bque += pd;
                        }

                        png_write_row( png_ptr, row );
                    }

                    png_write_end( png_ptr, NULL );

                    fclose( fp );

                    free(row);
                }

                png_free_data(png_ptr, info_ptr, PNG_FREE_ALL, -1);
                png_destroy_write_struct(&png_ptr, (png_infopp)NULL);

                return true;
            }
        }
    }
}
