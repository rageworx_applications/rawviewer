#ifndef __FL_TRANSBOX_H__
#define __FL_TRANSBOX_H__

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Image.H>

class Fl_TransBox : public Fl_Box
{
    public:
        Fl_TransBox(int x,int y,int w,int h, const char* l = 0);
        virtual ~Fl_TransBox();

    public:
        void color(unsigned int c);
        void set_alpha(unsigned char a);
        void set_dragEnabled(bool enabled) { dragEnabled = enabled; }

    protected:
        int handle(int e);
        void resize(int x, int y, int w, int h);
        void draw();

    private:
        void fill_buffer();
        void free_fl_rgb( Fl_RGB_Image* r );

    private:
        unsigned char*  buffer;
        unsigned char   r;
        unsigned char   g;
        unsigned char   b;
        unsigned char   alpha;
        Fl_RGB_Image*   img;
        bool            dragEnabled;
        const char*     refl;

};

#endif /// __FL_TRANSBOX_H__
