# Medical RAW viewer #

* An open source project of Medical RAW viewer with librawprocessor
* This is a part of open source project FLTK 1.3.4-1(-ts)

## Preview ##

* ![mrawviewer_v078216_preview.jpg](https://bitbucket.org/repo/9d9d6e/images/2696999427-mrawviewer_v078216_preview.jpg)

## Latest Update ##
* Version 0.7.12.232
* Supports loading 8bit gray scale ( not saves to 8bit ).
* Supports byte swap ( for read big-endian ).
* Some improves from graphical processing.

## Previous updates ##
* Fixed program not quits in normal way, it is maybe bug of FLTK.
* Replaced fl_smimg to using fl_imgtk library.
* Fixed an error, window components (Widgets) disabled when size change after image loaded.
* Updated to use latest version of librawprocessor.
* Updated to stability for drawing GUI on Windows system.
* Fixed a bugf failure when open by drag drop file(s).
* Now supporting Mac OS X application packaging.
* Fixed all bugs on Linux and Mac OS X
* Now supporting build on Mac OS X with console build (experimental)
* Update sources to compatible with latest version of librawprocessor and libtinydicom.
* Changed some reading sequence if source is DCM file.
* Updated librawprocessor
* Updated libtinydicom

## Supported features ##

* Read 8/12/14/16bit gray scaled raw (littel endian, big endian) images with options.
* Read DCM file
* Check pixel level matrix click on right mouse button.
* Check real pixel distance with degree with shift+left mouse click.
* Histogram view.
* Threshold window range view by click left and right mouse button. left button = minimal, right button = maximum.
* Exporting raw and png (8bit) image file.
* Resize 10% to 500%.
* Resize engine supports Bilinear, Bicubic, Lanczos3 filtering for exporting raw, png.

## Will embody ##

* Point to points, polygon region histogram view.
* High Dynamic tone mapping with Drago, Reinhard.
* And more.

## Requirements ##

* GCC/MinGW-W64 6.2 (w/ OpenMP, pthread) 32/64bit
* (if Windows)M-SYS 1.x 
* Open source libraries:
    1. FLTK ( http://fltk.org/ ), FLTK v1.3.4-1-ts ( https://github.com/rageworx/fltk-1.3.4-1-ts )
    1. librawprocessor ( https://github.com/rageworx/librawprocessor )
    1. libtinydicom ( https://github.com/rageworx/libtinydicom )
    1. minIni, Copyright (c) CompuPhase, 2008-2012
       http://www.apache.org/licenses/LICENSE-2.0

## License limits ##

* MIT for libraries - librawprocessor, libtinydicom
* GPL3 for this project.
